------------------------------------------------------------------------------------------------------------------------------------
-- IMPORT section
------------------------------------------------------------------------------------------------------------------------------------
import XMonad (mod4Mask, xK_F1, gets, io, spawn, (|||), xmonad, (-->), (<+>), (<||>), (=?),
               className, composeAll, doFloat, doIgnore, resource, title, kill, sendMessage,
               windows, withFocused, KeyMask, KeySym, Default(def),
               XConfig(terminal, focusFollowsMouse, clickJustFocuses, borderWidth, modMask,
                       workspaces, normalBorderColor, focusedBorderColor, layoutHook, manageHook,
                       handleEventHook, logHook, startupHook),
               XState(windowset), ChangeLayout(NextLayout), Full(Full), IncMasterN(IncMasterN),
               Mirror(Mirror), Resize(Expand, Shrink), Layout (Layout), ManageHook)
-- Action
import XMonad.Actions.CycleWS (nextScreen, nextWS, prevScreen, prevWS, WSType(WSIs))
import XMonad.Actions.MouseResize ( mouseResize, MouseResize )
import XMonad.Actions.WithAll (sinkAll)
-- Core
import XMonad.Core (io, Query, spawn, WindowSet, X, XConfig(terminal, focusFollowsMouse,
                                                            clickJustFocuses, borderWidth, modMask,
                                                            workspaces, normalBorderColor,
                                                            focusedBorderColor, layoutHook,
                                                            manageHook, handleEventHook, logHook,
                                                            startupHook),
                    XState(windowset))
-- Data
import Data.Char (isSpace, toUpper)
import Data.Maybe (isJust)
import Data.Monoid ()
-- Graphics
--import Graphics.X11.ExtraTypes.XF86
import Graphics.X11.Types (KeySym, xK_F1, mod4Mask, Window, KeyMask)
import Graphics.X11.Xlib.Types (Dimension)
import Graphics.X11.Xlib.Extras (Event)
-- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops (ewmh)
import XMonad.Hooks.ManageDocks (avoidStruts, AvoidStruts, docks, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (doCenterFloat, doFullFloat, isFullscreen)
import XMonad.Hooks.StatusBar.PP (def, dynamicLogWithPP, filterOutWsPP, shorten, wrap, xmobarColor,
                                  xmobarPP, PP(ppOutput, ppCurrent, ppVisible, ppHidden,
                                               ppHiddenNoWindows, ppTitle, ppSep, ppUrgent,
                                               ppExtras, ppOrder))
import XMonad.Hooks.WindowSwallowing (swallowEventHook)
-- Layout
import XMonad.Layout (Choose)
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.LayoutModifier (ModifiedLayout)
import XMonad.Layout.LimitWindows (limitWindows, LimitWindows)
import XMonad.Layout.MultiToggle (EOT, HCons, mkToggle, MultiToggle, single)
import XMonad.Layout.MultiToggle.Instances (StdTransformers(MIRROR))
import XMonad.Layout.NoBorders (smartBorders, SmartBorder)
import XMonad.Layout.Renamed (renamed, Rename(Replace))
import XMonad.Layout.ResizableTile (ResizableTall(ResizableTall))
import XMonad.Layout.ShowWName (def, showWName', SWNConfig(swn_font, swn_fade, swn_bgcolor, swn_color))
import XMonad.Layout.Simplest (Simplest(Simplest))
import XMonad.Layout.Spacing (decScreenSpacing, decWindowSpacing, incScreenSpacing,
                              incWindowSpacing, spacingRaw, Border(Border), Spacing)
import XMonad.Layout.SubLayouts (subLayout, Sublayout)
import XMonad.Layout.WindowNavigation (windowNavigation, WindowNavigation)
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
-- System
import System.Exit (exitSuccess)
import System.IO (hClose, hPutStr, hPutStrLn)
-- Utilities
import XMonad.Util.EZConfig (mkNamedKeymap)
import XMonad.Util.Hacks (javaHack, windowedFullscreenFixEventHook)
import XMonad.Util.NamedActions (NamedAction(..), (^++^), addDescrKeys, addName, showKmSimple)
import XMonad.Util.NamedScratchpad (customFloating, namedScratchpadAction, namedScratchpadManageHook,
                                    scratchpadWorkspaceTag, NamedScratchpad(NS))
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.SpawnOnce (spawnOnce)
-- Others
import qualified XMonad.StackSet as W

------------------------------------------------------------------------------------------------------------------------------------
-- Variable definition for easier modifications
------------------------------------------------------------------------------------------------------------------------------------

-- Preferred/default apps
-- terminal emulator
myTerminal :: String
myTerminal = "alacritty"
-- web browser
myWeBrowser :: String
myWeBrowser = "firefox"
-- file manager
myFileManager :: String
myFileManager = "thunar"
-- editor
myEditor :: String
myEditor = "nvim"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = False

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
myBorderWidth :: Graphics.X11.Xlib.Types.Dimension
myBorderWidth   = 2

-- modMask lets you specify which modkey you want to use. The default is mod1Mask ("left alt").
-- Other modkeys are mod3Mask -> "right alt" and mod4Mask -> "windows key".
myModMask :: KeyMask
myModMask = mod4Mask

-- The default number of workspaces
myWorkspaces :: [String]
myWorkspaces = [" \61612 ", " \61563 ", " \61831 ", " \61515 ", " \62141 "]

-- Counter of number of windows in active workspace
windowCount :: XMonad.Core.X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

-- Border colors for unfocused and focused windows, respectively.
myNormalBorderColor :: String
myNormalBorderColor = "#282C34"
myFocusedBorderColor :: String
myFocusedBorderColor = "#46D9FF"

-- Screenshot, windowshot and selectionshot terminal commands
maimU :: String
maimU = "maim -u ~/Pictures/$(date +%F-%T).png"
maimUI :: String
maimUI = "maim -ui $(xdotool getactivewindow) ~/Pictures/$(date +%F-%T).png"
maimUS :: String
maimUS = "maim -us ~/Pictures/$(date +%F-%T).png"

-- Run bash script to change the volume and show the notification
volMute :: [Char]
volMute = "sh ~/.local/bin/volume mute"
volUp :: String
volUp = "sh ~/.local/bin/volume up"
volDown :: String
volDown = "sh ~/.local/bin/volume down"

-- Display/monitor settings
displayL :: String
displayL = " --output DP-0 --primary --mode 2560x1440 --pos 0x586 --rate 165 --rotate normal"
displayR :: String
displayR = " --output DP-2 --mode 2560x1440 --pos 2560x0 --rate 165 --rotate left"
------------------------------------------------------------------------------------------------------------------------------------
-- Key bindings
------------------------------------------------------------------------------------------------------------------------------------

subtit ::  String -> ((KeyMask, KeySym), NamedAction)
subtit x = ((0,0), NamedAction $ map toUpper $ sep ++ "\n-- " ++ x ++ " --\n" ++ sep)
  where
    sep = replicate (6 + length x) '-'

showKeybindings :: [((KeyMask, KeySym), NamedAction)] -> NamedAction
showKeybindings x = addName "Show Keybindings" $ io $ do
  h <- spawnPipe "yad --text-info --fontname=\"MesloLGS NF 12\" --fore=#46D9FF back=#282C36 --center --geometry=1200x800 --title \"XMonad keybindings\""
  hPutStr h (unlines $ showKmSimple x) -- showKmSimple doesn't add ">>" to subtitles
  hClose h
  return ()

myKeys :: XConfig l0 -> [((KeyMask, KeySym), NamedAction)]
myKeys c = let subKeys str ks = subtit str : mkNamedKeymap c ks in

  subKeys "Xmonad Basic"
  [("M-r"  , addName "Restart xmonad/bar" $ spawn "killall xmobar; xmonad --recompile; xmonad --restart"),
   ("M-S-r", addName "Restart monitors"   $ spawn ("xrandr" ++ displayL ++ displayR)                    ),
   ("M-S-q", addName "Quit xmonad"        $ io exitSuccess                                              )]

  ^++^ subKeys "Favorite apps"
  [("M-<Return>", addName "Launch a terminal"    $ spawn myTerminal                        ),
   ("M-<Space>" , addName "Launch rofi instance" $ spawn "rofi -show drun"                 ),
   ("M-e"       , addName "Launch text editor"   $ spawn (myTerminal ++ " -e " ++ myEditor)),
   ("M-f"       , addName "Launch file manager"  $ spawn myFileManager                     ),
   ("M-w"       , addName "Launch web browser"   $ spawn myWeBrowser                       )]

  ^++^ subKeys "Windows"
  [("M-q"         , addName "Close focused window"               kill                           ),
   ("M1-<Tab>"    , addName "Switch to next layout"              $ sendMessage NextLayout       ),
   ("M-b"         , addName "Toggle the status bar gap"          $ sendMessage ToggleStruts     ),
   -- Focus
   ("M-<Tab>"     , addName "Focus to next window"               $ windows W.focusDown          ),
   ("M-j"         , addName "Focus to next window"               $ windows W.focusDown          ),
   ("M-k"         , addName "Focus to previous window"           $ windows W.focusUp            ),
   ("M-m"         , addName "Focus to master window"             $ windows W.focusMaster        ),
   -- Swap
   ("M-S-<Return>", addName "Swap focused and master window"     $ windows W.swapMaster         ),
   ("M-S-j"       , addName "Swap focused with next window"      $ windows W.swapDown           ),
   ("M-S-k"       , addName "Swap focused with previous window"  $ windows W.swapUp             ),
   -- Resize
   ("M-h"         , addName "Shrink the master area"             $ sendMessage Shrink           ),
   ("M-l"         , addName "Expand the master area"             $ sendMessage Expand           ),
   ("M-t"         , addName "Sink focused window into tiling"    $ withFocused $ windows.W.sink ),
   ("M-S-t"       , addName "Sink all floated windows"           sinkAll                        ),
   -- Change # of windows in master area of focused screen
   ("M-,"         , addName "Increment # windows in master area" $ sendMessage (IncMasterN 1)   ),
   ("M-."         , addName "Decrement # windows in master area" $ sendMessage (IncMasterN (-1))),
   -- Gaps
   ("C-M1-j"      , addName "Decrease window spacing"            $ decWindowSpacing 4           ),
   ("C-M1-k"      , addName "Increase window spacing"            $ incWindowSpacing 4           ),
   ("C-M1-h"      , addName "Decrease screen spacing"            $ decScreenSpacing 4           ),
   ("C-M1-l"      , addName "Increase screen spacing"            $ incScreenSpacing 4           )]

  ^++^ subKeys "Volume keybindings"
  [("<XF86AudioMute>"       , addName "Mute sound"       $ spawn volMute															),
   ("<XF86AudioNext>"       , addName "Next audio"       $ spawn "playerctl next"       							),
   ("<XF86AudioPrev>"       , addName "Previous audio"   $ spawn "playerctl previous"   							),
   ("<XF86AudioPlay>"       , addName "Play/pause audio" $ spawn "playerctl play-pause" 							),
   ("<XF86AudioRaiseVolume>", addName "Volume up"        $ spawn volUp                  							),
   ("<XF86AudioLowerVolume>", addName "Volume down"      $ spawn volDown                							),
   ("M-S-p"									, addName "Print screen"		 $ spawn maimU																),
   ("M-S-w"									, addName "Take windowshot"  $ spawn maimUI																),
   ("M-S-s"									, addName "Take selectshot"  $ spawn maimUS																),
   ("C-M1-<Delete>"         , addName "Power menu"       $ spawn "sh ~/.local/bin/system-power-action")]

  ^++^ subKeys "Monitors & workspaces"
  [("M-<Right>", addName "Focus to next monitor"   nextScreen),
   ("M-<Left>" , addName "Focus to prev monitor"   prevScreen),
   ("M-<Up>"   , addName "Focus to next workspace" nextWS    ),
   ("M-<Down>" , addName "Focus to prev workspace" prevWS    )]

  ^++^ subKeys "Layouts" -- (does nothing because "floats" layout is not defined)
  [("M-S-f", addName "Toggle float layout" $ sendMessage (T.Toggle "floats"))]

  ^++^ subKeys "Scratchpad"
  [("M-c"          , addName "Toggle scratchpad calculator" $ namedScratchpadAction myScratchPads "calculator"),
   ("M-s"          , addName "Toggle scratchpad music"      $ namedScratchpadAction myScratchPads "music"     ),
   ("M-M1-<Return>", addName "Toggle scratchpad terminal"   $ namedScratchpadAction myScratchPads "terminal"  )]

------------------------------------------------------------------------------------------------------------------------------------
-- Layouts: (each layout is separated by |||)
------------------------------------------------------------------------------------------------------------------------------------

--Makes setting the spacingRaw simpler to write
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

myLayout :: ModifiedLayout
  MouseResize
  (ModifiedLayout
     XMonad.Hooks.ManageDocks.AvoidStruts
     (XMonad.Layout.Choose
        (ModifiedLayout
           Rename
           (ModifiedLayout
              XMonad.Layout.LimitWindows.LimitWindows
              (ModifiedLayout
                 XMonad.Layout.NoBorders.SmartBorder
                 (ModifiedLayout
                    XMonad.Layout.WindowNavigation.WindowNavigation
                    (ModifiedLayout
                       (XMonad.Layout.SubLayouts.Sublayout
                          (ModifiedLayout XMonad.Layout.NoBorders.SmartBorder Simplest))
                       (ModifiedLayout Spacing ResizableTall))))))
        (XMonad.Layout.Choose
           (ModifiedLayout
              Rename
              (ModifiedLayout
                 XMonad.Layout.LimitWindows.LimitWindows
                 (ModifiedLayout
                    XMonad.Layout.NoBorders.SmartBorder
                    (ModifiedLayout
                       XMonad.Layout.WindowNavigation.WindowNavigation
                       (ModifiedLayout
                          (XMonad.Layout.SubLayouts.Sublayout
                             (ModifiedLayout XMonad.Layout.NoBorders.SmartBorder Simplest))
                          (ModifiedLayout
                             Spacing
                             (XMonad.Layout.MultiToggle.MultiToggle
                                (XMonad.Layout.MultiToggle.HCons
                                   StdTransformers XMonad.Layout.MultiToggle.EOT)
                                Grid)))))))
           (ModifiedLayout
              Rename
              (ModifiedLayout
                 XMonad.Layout.NoBorders.SmartBorder
                 (ModifiedLayout
                    XMonad.Layout.WindowNavigation.WindowNavigation
                    (ModifiedLayout
                       (XMonad.Layout.SubLayouts.Sublayout
                          (ModifiedLayout XMonad.Layout.NoBorders.SmartBorder Simplest))
                       Full)))))))
  Graphics.X11.Types.Window
myLayout = mouseResize $ avoidStruts (vTiled ||| grid ||| sngl)
  where
    -- default tiling algorithm partitions the screen into two panes
    vTiled  = renamed [Replace "\984640"] $ limitWindows 5 $ smartBorders
      $ windowNavigation $ subLayout [] (smartBorders Simplest)
      $ mySpacing 4 $ ResizableTall nmaster delta ratio []

    hTiled  = renamed [Replace "LonG"] $ Mirror vTiled

    grid    = renamed [Replace "\984432"] $ limitWindows 6 $ smartBorders
      $ windowNavigation $ subLayout [] (smartBorders Simplest)
      $ mySpacing 4 $ mkToggle (single MIRROR) $ Grid (16/10)

    sngl  = renamed [Replace "\61640"] $ smartBorders $ windowNavigation
      $ subLayout [] (smartBorders Simplest) $ Full

    -- The default number of windows in the master pane
    nmaster = 1

    -- Default proportion of screen occupied by master pane
    ratio   = 1/2

    -- Percent of screen to increment by when resizing panes
    delta   = 3/100

------------------------------------------------------------------------------------------------------------------------------------
-- Standby apps (terminal, calculator, etc.)
------------------------------------------------------------------------------------------------------------------------------------

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "calculator" "qalculate-gtk"            (className =? "Qalculate-gtk") (customFloating $ W.RationalRect 0.3  0.25 0.4 0.5),
                  NS "music"      "youtube-music"            (className =? "YouTube Music") (customFloating $ W.RationalRect 0.05 0.05 0.9 0.9),
                  NS "terminal"   (myTerminal ++ " -t term") (title =? "term")              (customFloating $ W.RationalRect 0.05 0.05 0.9 0.9)]

-- The following lines are needed for named scratchpads (to define non-visible workspace )
  where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
        nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))

------------------------------------------------------------------------------------------------------------------------------------
-- Window rules
------------------------------------------------------------------------------------------------------------------------------------

-- To find the property name associated with a program, use > xprop | grep WM_CLASS and click on the client you're interested in.
-- To match on the WM_NAME, you can use 'title' in the same way that 'className' and 'resource' are used below.

myManageHook :: ManageHook
myManageHook = composeAll
    -- doCenterFloat
  [ className =? "Blueman-manager"                --> doCenterFloat,
    className =? "confirm"                        --> doCenterFloat,
    className =? "dialog"                         --> doCenterFloat,
    className =? "download"                       --> doCenterFloat,
    className =? "error"                          --> doCenterFloat,
    className =? "feh"                            --> doCenterFloat,
    className =? "file_progress"                  --> doCenterFloat,
    className =? "File-roller"                    --> doCenterFloat,
    className =? "Gimp-2.10"                      --> doCenterFloat,
    className =? "mpv"                            --> doCenterFloat,
    className =? "Nm-applet"                      --> doCenterFloat,
    className =? "Nm-connection-editor"           --> doCenterFloat,
    className =? "Nm-openconnect-auth-dialog"     --> doCenterFloat,
    className =? "Nvidia-settings"                --> doCenterFloat,
    className =? "Thunar-volman-settings"         --> doCenterFloat,
    className =? "Xarchiver"                      --> doCenterFloat,
    className =? "Yad"                            --> doCenterFloat,
    title     =? "Close Firefox"                  --> doCenterFloat,
    title     =? "Enter License - Sublime Merge"  --> doCenterFloat,
    title     =? "Enter License - Sublime Text"   --> doCenterFloat,
    title     =? "Enter name of file to save to…" --> doCenterFloat,
    title     =? "Extension Manager"              --> doCenterFloat,
    title     =? "File Operation Progress"        --> doCenterFloat,
    title     =? "Library"                        --> doCenterFloat,
    title     =? "Open Files"                     --> doCenterFloat,
    title     =? "Save File"                      --> doCenterFloat,
    title     =? "Select Folder"                  --> doCenterFloat,
    title     =? "Update - Sublime Text"          --> doCenterFloat,
    -- doFloat
    --className =? "mpv"                            --> doFloat,
    -- doIgnore
    resource  =? "desktop_window"                 --> doIgnore,
    resource  =? "kdesktop"                       --> doIgnore,
    -- doShift
    --className =? "firefox"                        --> doShift (myWorkspaces !! 0),
    --className =? "Gimp-2.10"                      --> doShift (myWorkspaces !! 3),
    --className =? "mpv"                            --> doShift (myWorkspaces !! 3),
    --className =? "Subl"                           --> doShift (myWorkspaces !! 1),
    --className =? "Sublime_merge"                  --> doShift (myWorkspaces !! 1),
    isFullscreen                                  --> doFullFloat ] <+> namedScratchpadManageHook myScratchPads

------------------------------------------------------------------------------------------------------------------------------------
-- User defined theme to prints current workspace when you change workspace
------------------------------------------------------------------------------------------------------------------------------------

myShowWNameTheme :: SWNConfig
myShowWNameTheme = def {swn_font    = "xft:Font Awesome 6 Free Solid:bold:size=60",
                        swn_fade    = 1.0,
                        swn_bgcolor = "#1C1F24",
                        swn_color   =  "#FFFFFF" }

------------------------------------------------------------------------------------------------------------------------------------
-- Event handling (EwmhDesktops users should change this to ewmhDesktopsEventHook)
------------------------------------------------------------------------------------------------------------------------------------

-- Defines a custom handler function for X Events. The function should return (All True) if the default
-- handler is to be run afterwards. To combine event hooks use mappend or mconcat from Data.Monoid.

myWindowedFullscreenFixEventHook = handleEventHook def <+> windowedFullscreenFixEventHook
mySwallowEventHook = swallowEventHook (className =? "Alacritty" <||> className =? "XTerm") (return True)
myEventHook = myWindowedFullscreenFixEventHook <+> mySwallowEventHook

------------------------------------------------------------------------------------------------------------------------------------
-- Startup hook (perform an arbitrary action each time xmonad starts or is restarted)
------------------------------------------------------------------------------------------------------------------------------------

myStartupHook :: X ()
myStartupHook = do
  --spawnOnce "xrandr --output DP-0 --primary --mode 2560x1440 --pos 0x0 --rate 165 --output DP-2 --mode 2560x1440 --right-of DP-0 --rate 165"
  spawnOnce "feh --no-fehbg --bg-center ~/.local/share/backgrounds/arch_2560x1440.png --bg-center ~/.local/share/backgrounds/arch.png"
  spawnOnce "picom"
  spawnOnce "volumeicon"
  spawnOnce "nm-applet"
  spawnOnce "blueman-applet"
  spawnOnce "dropbox"
  --spawnOnce "$HOME/.dropbox-dist/dropboxd" -- if we have multiple instances of dropbox
  spawn     "/usr/bin/thunar --daemon"
  --spawn     "/usr/bin/emacs --daemon"
  spawnOnce "trayer --edge top --align right --height 23 --width 33 --expand true --transparent\
					\ true --alpha 0 --tint 0x000000 --distance -130 --distancefrom left --iconspacing 10"
  --spawnOnce "xautolock -time 15 -locker slock"
  spawnOnce "alacritty -e /bin/bash ~/Downloads/test.sh"

------------------------------------------------------------------------------------------------------------------------------------
-- Run xmonad with the settings you specify.
------------------------------------------------------------------------------------------------------------------------------------

main :: IO ()
main = do
  xmproc0 <- spawnPipe "xmobar -x 0 $HOME/.config/xmobar/xmobarrc0"                                 -- xmobar on first/primary monitor (monitor #0)
 -- xmproc1 <- spawnPipe "xmobar -x 1 $HOME/.config/xmobar/xmobarrc1"                                 -- xmobar on second monitor (monitor #1)

  xmonad $ addDescrKeys ((mod4Mask, xK_F1), showKeybindings) myKeys $ ewmh $ javaHack $ docks $ def {
    -- simple stuff
    terminal            = myTerminal,
    focusFollowsMouse   = myFocusFollowsMouse,
    clickJustFocuses    = myClickJustFocuses,
    borderWidth         = myBorderWidth,
    modMask             = myModMask,
    workspaces          = myWorkspaces,
    normalBorderColor   = myNormalBorderColor,
    focusedBorderColor  = myFocusedBorderColor,

    -- hooks, layouts
    layoutHook          = showWName' myShowWNameTheme myLayout,
    manageHook          = myManageHook,
    handleEventHook     = myEventHook,
    logHook             = dynamicLogWithPP $ filterOutWsPP [scratchpadWorkspaceTag] $ xmobarPP {    -- workspace and layout details
        ppOutput          = \x -> hPutStrLn xmproc0 x,                                              -- show in xmobar on monitor #0 (primary)
 --                              >> hPutStrLn xmproc1 x,                                              -- show in xmobar on monitor #1 (remove comma in line above for multi-monitor setup)
        ppCurrent         = xmobarColor "#C678DD" "" . wrap                                         -- current workspace indicator in xmobar
                            "<box type=Bottom width=3 mb=0 color=#C678DD>" "</box>",
        ppVisible         = xmobarColor "#C678DD" "",                                               -- visible but not current workspace
        ppHidden          = xmobarColor "#51AFEF" "",                                               -- hidden workspace with windows
        ppHiddenNoWindows = xmobarColor "#BFBFBF" "",                                               -- hidden workspace without windows
        ppTitle           = xmobarColor "#DFDFDF" "" . shorten 75,                                  -- title of active workspace
        ppSep             = "<fc=#5B6268> <fn=2>|</fn> </fc>",                                      -- separator between workspaces
        ppUrgent          = xmobarColor "#FF6C6B" "" . wrap "!" "!",                                -- urgent workspace
        ppExtras          = [windowCount],                                                          -- # of windows on current workspace
        ppOrder           = \(ws:l:t:ex) -> [ws,l]++ex++[t] },                                      -- order of things defined in logHook above
    startupHook         = myStartupHook }
