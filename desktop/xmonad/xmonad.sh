#!/bin/bash

# Synchronizing package databases and install basic packages for xmonad from pkglist.txt
grep -Ev '^\s*(#|$)' /arch-install/wm/xmonad/pkglist.txt | sudo pacman --needed --ask 4 -Sy --noconfirm -

# Copy all configuration files to the home directory
rsync -r --exclude=.gitkeep /arch-install/wm/xmonad/.config/ "$HOME/.config/"
