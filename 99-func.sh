#!/bin/bash
# @file			functions.sh
# @brief		This script contains all the functions that will be called/needed by the other SHELL
#						scripts needed to install Arch Linux. To use them, this file has to be sourced.

#▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬
#																		         FUNCTIONS
#▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬

# @description	Set a general purpose password (user password, root password, etc.).
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
set_password() {
    read -rs -p "Enter password: " PASSWORD
    echo -ne "\n"
    read -rs -p "Verify password: " REPASSWORD
    echo -ne "\n"
    if [[ ! "$PASSWORD" == "$REPASSWORD" ]]; then
        echo -ne "ERROR! Passwords do not match. \n"
        set_password
    fi
}

# @description	Check if the script has been run as root. If not, exit with error 0.
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
root_check() {
    if [[ "$(id -u)" != "0" ]]; then
        echo -ne "ERROR! This script must be run under the 'root' user!\n"
        exit 0
    fi
}

# @description	Check if the script is run under Arch Linux Distro. If not, exit with error 0.
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
arch_check() {
    if [[ ! -e /etc/arch-release ]]; then
        echo -ne "ERROR! This script must be run in Arch Linux!\n"
        exit 0
    fi
}

# @description	Check if pacman is fully operational. If not, exit with error 0.
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
pacman_check() {
    if [[ -f /var/lib/pacman/db.lck ]]; then
echo "ERROR! Pacman is blocked."
echo -ne "If not running remove /var/lib/pacman/db.lck.\n"
        exit 0
    fi
}

# @description	Run previous three checks simultaniously.
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
background_checks() {
    root_check
    arch_check
    pacman_check
}

# @description	Renders a text based list of options that can be selected by the
#								user using up, down and enter keys and returns the chosen option.
#
# @args					list of options, maximum of 256
#									"opt1" "opt2" ...
# @return				selected index (0 for opt1, 1 for opt2, ...)
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
select_option() {

    # little helpers for terminal print control and key input
    ESC=$( printf "\033")
    cursor_blink_on()  { printf "$ESC[?25h"; }
    cursor_blink_off() { printf "$ESC[?25l"; }
    cursor_to()        { printf "$ESC[$1;${2:-1}H"; }
    print_option()     { printf "$2   $1 "; }
    print_selected()   { printf "$2  $ESC[7m $1 $ESC[27m"; }
    get_cursor_row()   { IFS=';' read -sdR -p $'\E[6n' ROW COL; echo "${ROW#*[}"; }
    get_cursor_col()   { IFS=';' read -sdR -p $'\E[6n' ROW COL; echo "${COL#*[}"; }
    key_input()         {
                        local key
                        IFS= read -rsn1 key 2>/dev/null >&2
                        if [[ $key = ""      ]]; then echo enter; fi;
                        if [[ $key = $'\x20' ]]; then echo space; fi;
                        if [[ $key = "k" ]]; then echo up; fi;
                        if [[ $key = "j" ]]; then echo down; fi;
                        if [[ $key = "h" ]]; then echo left; fi;
                        if [[ $key = "l" ]]; then echo right; fi;
                        if [[ $key = "a" ]]; then echo all; fi;
                        if [[ $key = "n" ]]; then echo none; fi;
                        if [[ $key = $'\x1b' ]]; then
                            read -rsn2 key
                            if [[ $key = [A || $key = k ]]; then echo up;    fi;
                            if [[ $key = [B || $key = j ]]; then echo down;  fi;
                            if [[ $key = [C || $key = l ]]; then echo right;  fi;
                            if [[ $key = [D || $key = h ]]; then echo left;  fi;
                        fi 
    }
    print_options_multicol() {
        # print options by overwriting the last lines
        local curr_col=$1
        local curr_row=$2
        local curr_idx=0

        local idx=0
        local row=0
        local col=0
        
        curr_idx=$((curr_col+curr_row*colmax))
        
        for option in "${options[@]}"; do

            row=$((idx/colmax))
            col=$((idx-row*colmax))

            cursor_to $((startrow+row+1)) $((offset*col+1))
            if [ $idx -eq $curr_idx ]; then
                print_selected $option
            else
                print_option $option
            fi
            ((idx++))
        done
    }

    # initially print empty new lines (scroll down if at bottom of screen)
    for opt; do printf "\n"; done

    # determine current screen position for overwriting the options
		local lastrow=$(get_cursor_row)
		# local lastcol=$(get_cursor_col)
    local startrow=$((lastrow-$#))
    # local startcol=1
    # local lines=$(tput lines)
    # local cols=$(tput cols) #to use full terminal window
    local cols=76
    local colmax=$1
		local rowmax=$(((${#options[@]}+colmax-1)/colmax))
    local offset=$((cols/colmax))

    # ensure cursor and input echoing back on upon a ctrl+c during read -s
    trap "cursor_blink_on; stty echo; printf '\n'; exit" 2
    cursor_blink_off

    local active_row=0
    local active_col=0
    while true; do
        print_options_multicol $active_col $active_row 
        # user key control
				case $(key_input) in
            enter)	break;;
							 up)	((active_row--));
                    if [ $active_row -lt 0 ]; then active_row=0; fi;;
						 down)	((active_row++));
										if [ $active_row -eq $rowmax ]; then ((active_row--));
										elif [ $((${#options[@]}%colmax)) -gt 0 ] &&
												 [ $active_row -eq $((rowmax-1)) ] &&
												 [ $active_col -ge $((${#options[@]}%colmax)) ]; then
																						 ((active_row--))
										fi;;
             left)	((active_col--));
                    if [ $active_col -lt 0 ]; then active_col=0; fi;;
            right)	((active_col++));
                    if [ $active_col -eq $colmax ]; then ((active_col--));
										elif [ $active_row -eq $((rowmax-1)) ] &&
												 [ $active_col -eq $((${#options[@]}%colmax)) ]; then
																						 ((active_col--))
										fi;;
        esac
    done

    # cursor position back to normal
    cursor_to $lastrow
    printf "\n"
    cursor_blink_on

    return $((active_col+active_row*colmax))
}

# @description	Center text to a certain amount of spaces.
# @args					number of spaces, text
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
center () {
	if [ ${#2} -lt "$1" ]; then
		STR=$(printf "%*s%s" $((($1-${#2}+1)/2)) '' "$2")
	else
		STR=$2
	fi
}

# @description	Displays ARCHLINUX INSTALLER logo and place the instructions per selection beneath.
# @args					Text/instructions to write beneath ARCHLINUX INSTALLER
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
logo () {
	# Clear every message that was on the screen
	clear
	# Calculate the numebr of empty spaces to center the input text from argument $1
	BLINE="▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬"
	SLINE="――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――"
	# This will be shown on every set as user is progressing
	echo $BLINE
	echo "

    █████╗ ██████╗  ██████╗██╗  ██╗██╗     ██╗██╗  ██╗██╗   ██╗██╗  ██╗
   ██╔══██╗██╔══██╗██╔════╝██║  ██║██║     ██║███╗ ██║██║   ██║╚██╗██╔╝
   ███████║██████╔╝██║     ███████║██║     ██║██╔█╗██║██║   ██║ ╚███╔╝ 
   ██╔══██║██╔══██╗██║     ██╔══██║██║     ██║██║╚███║██║   ██║ ██╔██╗ 
   ██║  ██║██║  ██║╚██████╗██║  ██║███████╗██║██║ ╚██║╚██████╔╝██╔╝╚██╗
   ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝
   ██╗██╗  ██╗███████╗████████╗ █████╗ ██╗     ██╗     ███████╗██████╗ 
   ██║███╗ ██║██╔════╝╚══██╔══╝██╔══██╗██║     ██║     ██╔════╝██╔══██╗
   ██║██╔█╗██║███████╗   ██║   ███████║██║     ██║     ██████╗ ██████╔╝
   ██║██║╚███║╚════██║   ██║   ██╔══██║██║     ██║     ██╔═══╝ ██╔══██╗
   ██║██║ ╚██║███████║   ██║   ██║  ██║███████╗███████╗███████╗██║  ██║
   ╚═╝╚═╝  ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚══════╝╚══════╝╚═╝  ╚═╝"
	echo $BLINE
	IFS='&' read -ra LINES <<< "$1"
	for LINE in "${LINES[@]}"; do
		center ${#BLINE} "$LINE" 
		echo "$STR"
	done
	echo $SLINE
}

# @description		Set question or instruction and gives possible options.
# @args						$1			- number of columns for possible options (input argument)
#									$2			- question or instruction (input argument)
#									$3			- selected option (output argument)
#									${@:4}	- all other argument are possible options (input argument)
selection () {
	local -n CHOICE="$3"
	logo "$2"
	local OPTIONS=("${@:4}")
	select_option "$1" "${OPTIONS[@]}"
	CHOICE="${OPTIONS[$?]}"
}

# @description	Ask question where the answer in either YES or NO.
# @argrs				Answer (YES or NO); Question
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
question () {
	options=("YES" "NO")
	selection 1 "$1" YESNO "${options[@]}"
}

# @description	This function will handle file systems. At this movement,
#								we are handling onlybtrfs and ext4. Others will be added in future.
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
filesystem () {
	STR="Select the file system for root partition."
	options=("ext4" "btrfs")
	selection 2 "$STR" FSTYPE "${options[@]}"
	echo -e " To avoid encrypting the root partition, simply press \n RETURN button without entering password beforehand.\n"
	set_password
}

# @description	Detects and sets timezone.
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
timezone () {
	read -p "$1" TIMEZONE
	while IFS= read -r ZONE; do
		if [ "$TIMEZONE" == "$ZONE" ]; then
			break
		fi
	done <<< "$(timedatectl list-timezones)"
	if [ -z "$ZONE" ]; then
		timezone "Incorrect entry. Try again, e.g. Europe/London: "
	fi
}

# @description	Set user's keyboard mapping.
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
keymap () {
	STR="Select keyboard layout."
	options=("American-English" "British-English" "Deutsch" "Dvorak"\
		"Español" "Français" "Srpski" "Italiano" "Nederlands" "Pусский")
	selection 3 "$STR" KEYMAP "${options[@]}"
	case "$KEYMAP" in
		American-English)	KEYMAP="us";;
		 British-English) KEYMAP="uk";;
						 Deutsch) KEYMAP="de";;
							Dvorak) KEYMAP="dvorak";;
						 Español) KEYMAP="es";;
						Français) KEYMAP="fr";;
							Srpski) KEYMAP="hr";;
						Italiano) KEYMAP="it";;
					Nederlands) KEYMAP="nl";;
						 Pусский) KEYMAP="ru";;
	esac
}

# @description	Disk selection for drive to be used with
#								installation and check if the drive is an SSD.
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
diskpart () {
	center 74 "$1"
	options=($(lsblk -n --output TYPE,KNAME,SIZE | awk '$1=="disk"{print "/dev/"$2"|"$3}'))
	selection 1 "$1" DISK "${options[@]}"
	DISK=${DISK%|*}
}

# @description	Gather username, password and host/computer name to be used for installation.
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
userinfo () {
	logo "USER INFO"
	read -p "Enter your username: " UNAME
	set_password
	read -rep "Enter your hostname: " HNAME
}

# @description	Choose AUR helper.
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
aurhelper () {
  STR="Select AUR helper."
  options=("aura" "pacaur" "paru" "pikaur" "trizen" "yay" "none")
  selection 4 "$STR" AURHELPER "${options[@]}"
}

# @description Choose Desktop Environment
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
desktopenv () {
  STR="Select Desktop Enviroment."
	options=($(for dir in /mnt/arch-install/desktop/*/; do dir=${dir%*/}; echo "${dir##*/}"; done))
  selection 2 "$STR" DESKENV "${options[@]}"
}

# @description Choose whether to do full or minimal installation. 
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
installtype () {
	center 74 "Select the type of the installation."
  STR="        FULL - Fully featured desktop enviroment, with apps and themes
     MINIMAL - Only few selected apps to get you started
⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
$STR"
  options=("FULL" "MINIMAL")
  selection 1 "$STR" INSTYPE "${options[@]}"
}

# @description	Choose LINUX kernel.
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
kernel () {
  STR="Select desired kernel."
	options=("Stable" "Long-Term" "Hardened" "Zen")
  selection 2 "$STR" KERNEL "${options[@]}"
  case "$KERNEL" in
			 Stable) KERNEL="";;
		Long-Term) KERNEL="-lts";;
		 Hardened) KERNEL="-hardened";;
					Zen) KERNEL="-zen";;
	esac
}

# @description	Choose the NVIDIA GPU driver.
nvidia () {
	if [ -z "$1" ]; then
		STR="Select desired NVIDIA GPU driver."
		options=('Stable' 'DKMS')
		selection 2 "$STR" GPU "${options[@]}"
	  case "$GPU" in
				 Stable) GPU='nvidia{,-utils,-settings}';;
					 DKMS) GPU='nvidia-{dkms,utils,settings}';;
		esac
	elif [ "$1" == '-lts' ]; then
		STR="Select desired NVIDIA GPU driver."
		options=('Long-Term' 'DKMS')
		selection 2 "$STR" GPU "${options[@]}"
	  case "$GPU" in
			Long-Term) GPU='nvidia-{lts,utils,settings}';;
					 DKMS) GPU='nvidia-{dkms,utils,settings}';;
		esac
	else
		GPU='nvidia-{dkms,utils,settings}'
	fi
}
#▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬
