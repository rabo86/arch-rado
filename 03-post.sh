#!/bin/bash
# @file			03-post.sh
# @brief		This script will install the most basic packages using pacman, generate FSTAB,
#						copy the gitlab repository and run other scripts using archs-chroot.
#	@args			1: Timezone (e.g. Europe/Amsterdam)
#						2: Keyboard layout (us, dvorak, etc.)
#						3: User name
#						4: Password
#						5: Hostname/computer name
#						6: Kernel (stable, lts, hardened or zen)
#						7: GPU (amd, intel or nvidia)
#						8: PC type (desktop or laptop)
#						9: Audio drivers (PipeWire or PulseAudio)
#▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬
#																						SET VARIABLES
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
# Set the system time and sync the clock
sudo timedatectl set-ntp true
sudo hwclock --systohc

# Synchronize package databases and install basic packages from pkglist.txt
grep -Ev '^\s*(#|$)' /arch-install/wm/pkglist.txt | sudo pacman --needed --noconfirm --ask 4 -Sy -

# Install python packages from python-pkg.txt
pip3 install -r /arch-install/wm/python-pkg.txt

# Install AUR helper PARU
cd "$HOME/Downloads" || exit
git clone https://aur.archlinux.org/paru-bin.git
(cd paru-bin && makepkg -si --noconfirm)
rm -rf paru-bin
# Change some default PARU options
# sudo sed -i 's/#AurOnly/AurOnly/g' /etc/paru.conf       # Apply PARU just to AUR packages
sudo sed -i '/\[options\]/a SkipReview' /etc/paru.conf  # Skip Review during installation

# Refresh AUR repository, upgrade the system and install basic packages from pkglist-aur.txt
grep -Ev '^\s*(#|$)' /arch-install/wm/pkglist-aur.txt | paru --needed --ask 4 -Syyu --noconfirm -

# Enabling multilib repository for 32-bit software and libraries, refresh repositories and installing 32-bit packages
if [ $2 == "32" ]; then
	sudo sed -i '/multilib]$/,+1s/#//' /etc/pacman.conf
	grep -Ev '^\s*(#|$)' /arch-install/wm/pkglist-32bit.txt | sudo pacman --needed --ask 4 -Syyu --noconfirm -
fi

# Install OH-MY-ZSH, syntax-highlighting, autosuggestions and POWERLEVEL10K
no | sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

# Install NvChad Neovim theme and configuration
#git clone https://github.com/NvChad/NvChad ~/.config/nvim --depth 1

# Run wsettings.sh script to configure all necessary settings
sh /arch-install/wm/wsettings.sh

# Run scripts to set up Window Manager
if [ $1 == "openbox" ]; then
	sh /arch-install/wm/openbox/openbox.sh
elif [ $1 == "xmonad" ]; then
	sh /arch-install/wm/xmonad/xmonad.sh
fi

# Remove arch-install repository
sudo rm -rf /arch-install

echo -e '\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m'
sleep 5
reboot
