/**
 * Urban LifeStyle SDDM Theme
 *
 * @author Alfredo Ramos <alfredo.ramos@yandex.com>
 * @copyright 2015 Alfredo Ramos
 * @license GPL-3.0-or-later
 */

import QtQuick 2.6
import QtQuick.Layouts 1.2
import SddmComponents 2.0

import './components' as UrbanLifeStyleComponents

Rectangle {
	id: container
	width: 2560
	height: 1440
	color: "#FFF"

	TextConstants {
		id: textConstants
	}

	Connections {
		target: sddm

		onLoginFailed: {
			message.color = config.errColor
			message.font.bold = true
			message.text = textConstants.loginFailed
			password.text = ''
			password.focus = true
		}
	}

	Background {
		anchors.fill: parent
		anchors.centerIn: parent
		source: config.background
		fillMode: Image.PreserveAspectFit

		onStatusChanged: {
			if (status === Image.Error && source !== config.defaultBackground) {
				source = config.defaultBackground
			}
		}
	}

	Rectangle {
		id: loginBox
		anchors.left: parent.left
		anchors.top: parent.top
		anchors.topMargin: config.topMargin
		anchors.leftMargin: config.leftMargin
		width: config.boxWidth
		height: loginBoxLayout.implicitHeight + (loginBoxLayout.anchors.margins * 2)
		color: config.bckgrndColor
		border.color: config.borderColor
		border.width: config.borderWidth
		radius: config.cornerRadius

		GridLayout {
			id: loginBoxLayout
			anchors.fill: parent
			anchors.margins: 10
			columns: 3
			rows: 9
			z: 1

			// Welcome message
			Text {
				color: config.frgrndColor
				font.bold: true
				font.pixelSize: config.passFontSize
				text: textConstants.welcomeText.arg(sddm.hostName)
				horizontalAlignment: Text.AlignHCenter
				verticalAlignment: Text.AlignVCenter
				wrapMode: Text.WordWrap

				Layout.columnSpan: loginBoxLayout.columns
				Layout.fillWidth: true
				Layout.fillHeight: true
				Layout.bottomMargin: loginBoxLayout.anchors.margins / 2
			}

			// Avatar
			ListView {
				id: users
				model: userModel

				delegate: Image {
					anchors.fill: parent
					width: loginBox.width / 3
					height: width
					sourceSize.width: width
					sourceSize.height: height
					clip: true
					smooth: true
					asynchronous: true
					fillMode: Image.PreserveAspectFit
					source: icon
					property string avatarPath: icon.toString().replace(/(\w*\.face\.icon)/, '')

					onStatusChanged: {
						if (status === Image.Error) {
							source = config.avatar.arg(avatarPath).arg('')
						}
					}
				}

				Layout.rowSpan: 4
				Layout.fillWidth: true
				Layout.fillHeight: true
				Layout.topMargin: loginBoxLayout.anchors.margins / 4
				Layout.bottomMargin: Layout.topMargin
			}

			// Name label
			Text {
				color: color.frgrndColor
				font.bold: true
				font.pixelSize: config.labelFontSize
				text: textConstants.userName

				Layout.columnSpan: loginBoxLayout.columns - 1
				Layout.fillWidth: true
			}

			// Name field
			TextBox {
				id: name
				color: config.fldBrndColor
				font.bold: false
				font.pixelSize: config.nameFontSize
				text: userModel.lastUser
				focusColor: config.focNamColor
				hoverColor: focusColor

				Layout.columnSpan: loginBoxLayout.columns - 1
				Layout.fillWidth: true

				KeyNavigation.backtab: shutdownButton
				KeyNavigation.tab: password

				Keys.onPressed: {
					if (event.key === Qt.Key_Return || event.key === Qt.Key_Enter) {
						sddm.login(name.text, password.text, session.index)
						event.accepted = true
					}
				}

				Keys.onReleased: {
					users.currentItem.source = config.avatar.arg(users.currentItem.avatarPath).arg(name.text)
				}
			}

			// Password label
			Text {
				color: config.frgrndColor
				font.bold: true
				font.pixelSize: config.labelFontSize
				text: textConstants.password

				Layout.columnSpan: loginBoxLayout.columns - 1
				Layout.fillWidth: true
			}

			// Password field
			PasswordBox {
				id: password
				color: config.fldBrndColor
				font.bold: false
				font.pixelSize: config.passFontSize
				focusColor: config.focPasColor
				hoverColor: focusColor

				Layout.columnSpan: loginBoxLayout.columns - 1
				Layout.fillWidth: true

				KeyNavigation.backtab: name
				KeyNavigation.tab: session

				Keys.onPressed: {
					if (event.key === Qt.Key_Return || event.key === Qt.Key_Enter) {
						sddm.login(name.text, password.text, session.index)
						event.accepted = true
					}
				}
			}

			// Session label
			Text {
				color: config.frgrndColor
				font.bold: true
				font.pixelSize: config.labelFontSize
				text: textConstants.session

				Layout.columnSpan: loginBoxLayout.columns - 1
				Layout.fillWidth: true
			}

			// Keyboard layout label
			Text {
				color: config.frgrndColor
				font.bold: true
				font.pixelSize: config.labelFontSize
				text: textConstants.layout

				Layout.fillWidth: true
			}

			// Session field
			ComboBox {
				id: session
				color: config.fldBrndColor
				focusColor: config.fcsSesColor
				hoverColor: focusColor
				arrowIcon: config.angleDown
				model: sessionModel
				index: sessionModel.lastIndex
				z: 1

				Layout.columnSpan: loginBoxLayout.columns - 1
				Layout.fillWidth: true

				KeyNavigation.backtab: password
				KeyNavigation.tab: keyboardLayout
			}

			// Keyboard layout field
			LayoutBox {
				id: keyboardLayout
				color: config.fldBrndColor
				focusColor: config.fcsKeyColor
				hoverColor: focusColor
				arrowIcon: config.angleDown
				z: 1

				Layout.fillWidth: true

				KeyNavigation.backtab: session
				KeyNavigation.tab: loginButton
			}

			// Message
			Text {
				id: message
				color: config.frgrndColor
				font.pixelSize: config.messageFontSize
				text: textConstants.prompt
				horizontalAlignment: Text.AlignHCenter
				verticalAlignment: Text.AlignVCenter

				Layout.columnSpan: loginBoxLayout.columns
				Layout.fillWidth: true
				Layout.fillHeight: true
				Layout.topMargin: loginBoxLayout.anchors.margins / 2
				Layout.bottomMargin: Layout.topMargin
			}

			// Login button
			Button {
				id: loginButton
				color: config.logButColor
				font.pixelSize: config.labelFontSize
				activeColor: color
				text: textConstants.login

				onClicked: sddm.login(name.text, password.text, session.index)

				Layout.fillWidth: true

				KeyNavigation.backtab: keyboardLayout
				KeyNavigation.tab: rebootButton
			}

			// Reboot button
			Button {
				id: rebootButton
				color: config.rbtButColor
				font.pixelSize: config.labelFontSize
				activeColor: color
				text: textConstants.reboot

				onClicked: sddm.reboot()

				Layout.fillWidth: true

				KeyNavigation.backtab: loginButton
				KeyNavigation.tab: shutdownButton
			}
			
			// Shutdown button
			Button {
				id: shutdownButton
				color: config.shtButColor
				font.pixelSize: config.labelFontSize
				activeColor: color
				text: textConstants.shutdown

				onClicked: sddm.powerOff()

				Layout.fillWidth: true

				KeyNavigation.backtab: rebootButton
				KeyNavigation.tab: name
			}
		}

		// Clock
		UrbanLifeStyleComponents.CustomClock {
			width: loginBox.width
			anchors.top: loginBox.bottom
			anchors.topMargin: - config.aboveBox
			color: config.frgrndColor 
			timeFormat: config.timeFormat
			dateFormat: config.dateFormat
			timeFont.pixelSize: parseInt(config.timeFontSize)
			dateFont.pixelSize: parseInt(config.dateFontSize)
		}

	}

	Component.onCompleted: {
		if (name.text === '') {
			name.focus = true
		} else {
			password.focus = true
		}
	}
}
