# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
[[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]] && source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set the theme
ZSH_THEME="powerlevel10k/powerlevel10k"

# Set one of the optional three time stamp formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd.mm.yyyy"

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git archlinux zsh-syntax-highlighting zsh-autosuggestions)

source $ZSH/oh-my-zsh.sh

# USER CONFIGURATION

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ -f ~/.p10k.zsh ]] && source ~/.p10k.zsh

# Source the scripts if exist to alow optional FZF keybindings and completion
[[ -f /usr/share/fzf/key-bindings.zsh ]] && source /usr/share/fzf/key-bindings.zsh
[[ -f /usr/share/fzf/completion.zsh ]] && source /usr/share/fzf/completion.zsh

### EXPORT
export TERM=xterm-256color
export HISTORY_IGNORE="(cd|cd -|cd ..|clear|exit|history|ls|ls -la|pwd|reboot)"
export EDITOR="nvim"     # $EDITOR use NeoVim in terminal
export VISUAL="nvim"     # $VISUAL use NeoVim in GUI mode
# EXPORT INTEL VARIABLES
#source /opt/intel/oneapi/setvars.sh
#export LD_PRELOAD=/opt/intel/oneapi/mkl/2022.2.1/lib/intel64/libmkl_sequential.so:/opt/intel/oneapi/mkl/2022.2.1/lib/intel64/libmkl_core.so

# PATH
[[ -d "$HOME/.bin" ]] && PATH="$HOME/.bin:$PATH"
[[ -d "$HOME/.local/bin" ]] && PATH="$HOME/.local/bin:$PATH"
# OTHER ENVIRONMENT VARIABLES
[[ -z "$XDG_CONFIG_HOME" ]] && export XDG_CONFIG_HOME="$HOME/.config"
[[ -z "$XDG_DATA_HOME" ]] && export XDG_DATA_HOME="$HOME/.local/share"
[[ -z "$XDG_CACHE_HOME" ]] && export XDG_CACHE_HOME="$HOME/.cache"
# XMONAD VARIABLES
export XMONAD_CONFIG_DIR="${XDG_CONFIG_HOME:-$HOME/.config}/xmonad" # xmonad.hs is expected to stay here
export XMONAD_DATA_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/xmonad"
export XMONAD_CACHE_DIR="${XDG_CACHE_HOME:-$HOME/.cache}/xmonad"

# ADDITIONAL OPTIONS
# Ignore saving duplicats in the history
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
# Extension and Globbing. Treat the #, ~ and ^ characters 
# as part of patterns for filename generation, etc.
#setopt extended_glob

# Alias section
# eza
alias ls='eza --icons --color=always --group-directories-first'
# neovim
alias vim='nvim'
# powerlevel10k
alias up10k='git -C ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k pull'
# pacman and paru
alias update='paru -Syyu; omz update; up10k'	# refresh and update standard pkgs, AUR pkgs, oh-my-zsh and powerlevel10k
alias cleanup='yes | sudo pacman -Scc; paccache -r; sudo pacman -Rns --noconfirm $(pacman -Qtdq)' # remove cache & orphaned packages
# adding flags
alias df='df -h'			# human-readable sizes
alias free='free -m'	# show sizes in MB
