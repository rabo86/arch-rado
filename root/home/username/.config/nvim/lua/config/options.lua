------------------------------------------------------------------------
-- [[ Options ]]
------------------------------------------------------------------------
-- See ':help vim.o'

-- Disable NETRW (NOTE: Strongly advised at very start of your init.lua)
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- Relative line number
vim.opt.number = true
-- vim.opt.relativenumber = true

-- Tab spaces
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
-- vim.opt.expandtab = true
vim.opt.softtabstop = 4
vim.opt.smartindent = true

-- Disable wrap
vim.opt.wrap = false

-- UndoTree access to long running undo's
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv('HOME') .. '/.vim/undodir'
vim.opt.undofile = true

-- Disable search highlight and enable incremental search
vim.opt.hlsearch = false
vim.opt.incsearch = true

-- Color highlighting (parentheses, etc.)
vim.opt.termguicolors = true

-- Vertical line
vim.opt.colorcolumn = '80'

-- Fast update time
vim.opt.updatetime = 250
vim.opt.timeoutlen = 300

-- Configure how new split should be open
vim.opt.splitright = true
vim.opt.splitbelow = true

-- Set completeopt ho have a better completion experience
vim.opt.completeopt = 'menuone,noselect'

-- Define how to display certain white spaces
-- vim.opt.list = true
-- vim.opt.listchars = { tab = '» ', trail = '█', nbsp = '󱁐' }

-- Never show less then 3 columns before or after cursor
vim.opt.scrolloff = 5
vim.opt.signcolumn = 'yes'
vim.opt.isfname:append('@-@')

-- [[ Highlight on yank ]]
-- See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
	callback = function()
		vim.highlight.on_yank()
	end,
	group = highlight_group,
	pattern = '*'
})

-- Toggle between relative and absolute line-numbering
-- depending of the mode or buffer you are in:
-- 1) relative line-numbering: focused buffer in normal mode
-- 2) absolute line-numbering: all other combinations
local augroup = vim.api.nvim_create_augroup("numbertoggle", { clear = true })
vim.api.nvim_create_autocmd({ "BufEnter", "FocusGained", "InsertLeave", "CmdlineLeave", "WinEnter" }, {
	pattern = "*",
	group = augroup,
	callback = function()
		if vim.o.nu and vim.api.nvim_get_mode().mode ~= "i" then
			vim.opt.relativenumber = true
		end
	end,
})
vim.api.nvim_create_autocmd({ "BufLeave", "FocusLost", "InsertEnter", "CmdlineEnter", "WinLeave" }, {
	pattern = "*",
	group = augroup,
	callback = function()
		if vim.o.nu then
			vim.opt.relativenumber = false
			vim.cmd "redraw"
		end
	end,
})
