------------------------------------------------------------------------
--  Install LAZY
------------------------------------------------------------------------
local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system {
		'git',
		'clone',
		'--filter=blob:none',
		'https://github.com/folke/lazy.nvim.git',
		'--branch=stable', -- latest stable release
		lazypath
	}
end
vim.opt.rtp:prepend(lazypath)

------------------------------------------------------------------------
--  Import plugins
------------------------------------------------------------------------

-- PLUGINS
require('lazy').setup(
-- NOTE: Plugins can be installed directly here
-- or added to imported directories (e.g. plugins)
	'plugins',

	{
		defaults = {
			lazy		= false,
			version	= false
		},
		install = { missing	= true },
		checker = { enable	= true },
		ui = {
			icons = {
				cmd					= '',
				config			= '',
				event				= '',
				ft					= '',
				init				= '',
				keys				= '󱕴',
				lazy				= "󰂠 ",
				loaded			= "",
				not_loaded	= "",
				plugin			= '',
				runtime			= '󱫠',
				source			= '',
				start				= '',
				task				= '',
			}
		},
		performance = {
			rtp = {
				disabled_plugins = {
					"2html_plugin",
					"tohtml",
					"getscript",
					"getscriptPlugin",
					"gzip",
					"logipat",
					"netrw",
					"netrwPlugin",
					"netrwSettings",
					"netrwFileHandlers",
					"matchit",
					"tar",
					"tarPlugin",
					"rrhelper",
					"spellfile_plugin",
					"vimball",
					"vimballPlugin",
					"zip",
					"zipPlugin",
					"tutor",
					"rplugin",
					"syntax",
					"synmenu",
					"optwin",
					"compiler",
					"bugreport",
					"ftplugin",
					"editorconfig",
				},
			},
		},
	})
