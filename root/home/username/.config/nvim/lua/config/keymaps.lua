------------------------------------------------------------------------
-- [[ Keybindings ]]
------------------------------------------------------------------------
-- See ':help vim.keymap.set()'

--  Set <Space> as the LEADER key (NOTE: Must happen before plugins
--  are required, otherwise wrong LEADER will be used)
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- Move selected text up/down
vim.keymap.set('v', 'J', ":m '>+1<CR>gv=gv", { desc = 'Move selected text up'   })
vim.keymap.set('v', 'K', ":m '<-2<CR>gv=gv", { desc = 'Move selected text down' })

vim.keymap.set('n', 'J',			'mzJ`z',		{ desc = 'Keep cursor steady while appending the line below'	})
vim.keymap.set('n', '<C-d>',	'<C-d>zz',	{ desc = 'Half page jump down with cursor in the middle'			})
vim.keymap.set('n', '<C-u>',	'<C-u>zz',	{ desc = 'Half page jump up with cursor in the middle'				})

vim.keymap.set('n', 'n', 'nzzzv', { desc = 'Keep the search term in the middle while seaching down'	})
vim.keymap.set('n', 'N', 'Nzzzv', { desc = 'Keep the search term in the middle while seaching up'	})

-- greatest remap ever
vim.keymap.set({ 'n', 'v' },	'<LEADER>d', [["_d]] ,	{ desc = 'Delete into the void register'					})
vim.keymap.set('x',						'<LEADER>p', [["_dP]],	{ desc = 'Delete into the void register & paste'	})
vim.keymap.set({ 'n', 'v' },	'<LEADER>y', [["+y]],		{ desc = 'Copy to the system clipboard'						})

-- Deleting into the void register

-- Make sure [Q] does nothing in NORMAL mode and same as [q] in COMMAND mode
vim.keymap.set('n', 'Q', '<nop>', { desc = 'Capital [Q] does nothing'})
vim.keymap.set('c', 'Q', 'q', { desc = 'Uppercase [Q] is same as lowercase [q]'})

-- quick fix and navigation
--vim.keymap.set("n", "<C-k>", "<CMD>cnext<CR>zz")
--vim.keymap.set("n", "<C-j>", "<CMD>cprev<CR>zz")
--vim.keymap.set("n", "<LEADER>k", "<CMD>lnext<CR>zz")
--vim.keymap.set("n", "<LEADER>j", "<CMD>lprev<CR>zz")

-- Replace the word on cursor with custom word
-- vim.keymap.set('n', '<LEADER>r', [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]], {desc = 'Replace All' })
vim.keymap.set('n', '<LEADER>r', [[:%s/\<<C-r><C-w>\>//gI<Left><Left><Left>]], { desc = 'Replace All' })

-- Buffer
vim.keymap.set('n', '<LEADER>bn', vim.cmd.enew, { desc = 'New'							})
vim.keymap.set('n', '<LEADER>bh', vim.cmd.new,	{ desc = 'New (Horizontal)'	})
vim.keymap.set('n', '<LEADER>bv', vim.cmd.vnew, { desc = 'New (Vertical)'		})

-- Diagnostic
vim.keymap.set('n', '<LEADER>df', vim.diagnostic.open_float,	{ desc = 'Open Float message'			})
vim.keymap.set('n', '<LEADER>dn', vim.diagnostic.goto_next,		{ desc = 'Go to Next message'			})
vim.keymap.set('n', '<LEADER>dp', vim.diagnostic.goto_prev,		{ desc = 'Go to Previous message'	})

-- Lazy
vim.keymap.set('n', '<LEADER>l', vim.cmd.Lazy, { desc = 'Lazy' })

-- Add
vim.keymap.set('n', '<LEADER>a', '<CMD><CR>', { desc = 'Add' })
