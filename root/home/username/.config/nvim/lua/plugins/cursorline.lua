-- [[ Install ]]
return {
	'yamatsum/nvim-cursorline',
	-- [[ Configure ]]
	config = function()
		require('nvim-cursorline').setup {
			cursorline = {
				enable	= true,
				timeout	= 250,
				number	= false,
			},
			cursorword = {
				enable			= true,
				min_length	= 1,
				hl					= { underline = true },
			}
		}
	end
}
