-- [[ Install Fuzzy Finder ]]
return {
	'nvim-telescope/telescope.nvim',
	branch = '0.1.x',
	-- cmd = 'Telescope',
	dependencies = {
		'nvim-lua/plenary.nvim',
		'nvim-telescope/telescope-file-browser.nvim', -- add keybindings
		{ 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
		'nvim-telescope/telescope-ui-select.nvim',
		'folke/which-key.nvim'
	},
	-- [[ Configure Telescope ]]
	config = function()
		require('telescope').setup {
			defaults = {
				vimgrep_arguments		= {
					'rg',
					'-L',
					'--color=never',
					'--no-heading',
					'--with-filename',
					'--line-number',
					'--column',
					'--smart-case',
				},
				prompt_prefix					= '  ',
				selection_caret				= '  ',
				multi_icon						= '',
				-- sorting_strategy			= 'ascending',
				-- Constant that multiplies vim.o.lines in the line below is the ration between the height
				-- and width of a single character field/place. It depends of the font type and font size.
				-- For this particular case (font: MelsoLGS NF; size: 12) the value is 2.2.
				layout_strategy				= (2.2*vim.o.lines < vim.o.columns) and 'horizontal' or 'vertical',
				layout_config					= {
					horizontal = {
						-- prompt_position = 'top',
					},
					vertical = {
						prompt_position = 'top',
					},
				},
				file_ignore_patterns	= { 'node_modules' },
				path_display					= { 'truncate' },
				set_env								= { ['COLORTERM'] = 'truecolor' }, -- default = nil,
				borderchars						= { '═', '│', '═', '│', '╒', '╕', '╛', '╘' },
				mappings							= {
					i = { ['<Tab>'] = require('telescope.actions').move_selection_next },
					n = { ['<Tab>'] = require('telescope.actions').move_selection_next }
				}
			},
			-- pickers = {},
			extensions = {
				['ui-select'] = {
					require('telescope.themes').get_dropdown(),
				}
			}
		}

		-- Enable plugins if installed
		-- tesescope-file-browser
		require('telescope').load_extension('file_browser')
		-- telescope-fzf-native
		require('telescope').load_extension('fzf')
		-- telescope-ui-select
		require('telescope').load_extension('ui-select')
		-- teselcope-noice
		-- require('telescope').load_extension('noice')

		-- Keybindings
		local builtin = require('telescope.builtin')
		require("which-key").register({
			f = { name = 'Find',
				b = { builtin.buffers,		'Buffer'		},
				c = { function() builtin.current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
									winblend = 10,
									previewer = false,
								})
							end,	'Current Buffer'	},
				f = { builtin.find_files,	'Files'			},
				g = { builtin.live_grep,	'Grep'			},
				h = { builtin.help_tags,	'Help'			},
				k = { builtin.keymaps,		'Keymaps'		},
				m = { builtin.resume,			'Resume'		},
				n = { function() builtin.find_files {
									cwd = vim.fn.stdpath 'config'
								}
							end,	'Neovim Config Files'		},
				o = { function() builtin.live_grep {
									grep_open_files = true,
									prompt_title = 'Live Grep on Open Files'
								}
							end,	'Grep on Open Files'		},
				r = { builtin.oldfiles,		'Recent'		},
				s = { builtin.treesitter,	'Symbols'		},
				t = { builtin.git_files,	'Git Files'	},
				w = { builtin.grep_string,'Word'			},
			}
		}, { prefix = "<LEADER>" })
	end
}
