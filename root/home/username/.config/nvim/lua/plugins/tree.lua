-- [[ Install ]]
return {
	'nvim-tree/nvim-tree.lua',
	-- cmd = 'Tree',
	keys = {
		{ '<LEADER>tt', vim.cmd.NvimTreeToggle,   desc = 'Toggle'				},
		{ '<LEADER>tf', vim.cmd.NvimTreeFocus,    desc = 'Focused'			},
		{ '<LEADER>ts', vim.cmd.NvimTreeFindFile, desc = 'Search File'	},
		{ '<LEADER>tc', vim.cmd.NvimTreeCollapse, desc = 'Collapse'			},
	},
	dependencies = { 'nvim-tree/nvim-web-devicons' },
	-- [[ Configure ]]
	opts = {
		-- Change some of default icons
		diagnostics	= {
			icons = {
				hint = ''
			}
		},
		renderer	= {
			root_folder_label	= ':~:s?$?/...?',
			indent_markers		= {
				enable = true,
				icons	= {
					corner	= '┗',
					edge		= '┃',
					item		= '┣',
					bottom	= '-',
				},
			},
			icons							= {
				git_placement	= 'after',
				symlink_arrow	= '  ',
				show					= {
					folder_arrow	= false,
					git						= true,
					modified			= true
				},
				glyphs = {
					modified			= '',
					folder				= {
						default			= '󰉋',
						open				= '',
						empty				= '',
						empty_open	= '',
					},
					git					= {
						unstaged	= '✘',
						staged		= '✔',
						unmerged	= '',
						renamed		= '󰑕',
						untracked	= '',
						deleted		= '󰚃',
						ignored		= '',
					}
				}
			}
		},
		git			= {
			ignore = false
		},
		modified	= { enable = true }
	}
}
