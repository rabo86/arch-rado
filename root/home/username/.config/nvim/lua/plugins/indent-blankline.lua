return {
	-- [[ Install ]]
	'lukas-reineke/indent-blankline.nvim',
	main = 'ibl',
	keys = {
		{ '<LEADER>ie', vim.cmd.IBLEnable, desc = 'Enable' },
		{ '<LEADER>it', vim.cmd.IBLToggle, desc = 'Toggle' }
	},
	-- [[ Configure ]]
	opts = {
		indent			= { char = '│' },
		whitespace	= { remove_blankline_trail = false }
	}
}
