-- [[ Install ]]
return {
	'airblade/vim-gitgutter',
	dependencies = {
		'folke/which-key.nvim'
	},
	-- cmd = 'GitGutter',
	ft = { 'gitcommit', 'diff' }, -- git filetypes
	init = function()
		-- Load GitGutter only when a git file is opened
		vim.api.nvim_create_autocmd({ 'BufRead' }, {
			group = vim.api.nvim_create_augroup('GitGutterLazyLoad', { clear = true }),
			callback = function()
				vim.fn.system('git -C ' .. '"' .. vim.fn.expand '%:p:h' .. '"' .. ' rev-parse')
				if vim.v.shell_error == 0 then
					vim.api.nvim_del_augroup_by_name 'GitGutterLazyLoad'
					vim.schedule(function()
						-- Load GitGutter
						require('lazy').load { plugins = { 'vim-gitgutter' } }
						-- Configure
						vim.cmd('let g:gitgutter_floating_window_options["border"] = "double"')
						vim.cmd('let g:gitgutter_close_preview_on_escape = 1')
					end)
				end
			end,
		})
	end,
	-- [[ Keybindings ]]
	config = function()
		require("which-key").register({
			-- Buffer
			g = { name = 'Git',
				d = { vim.cmd.GitGutterDiffOrig,		'Diff'					},
				f = { vim.cmd.GitGutterFold,				'Fold'					},
				n = { vim.cmd.GitGutterNextHunk,		'Next Hunk'			},
				p = { vim.cmd.GitGutterPrevHunk,		'Previous Hunk' },
				s = { vim.cmd.GitGutterStageHunk,		'Stage Hunk'		},
				t = { vim.cmd.GitGutterToggle,			'Toggle'				},
				u = { vim.cmd.GitGutterUndoHunk,		'Undo Hunk'			},
				v = { vim.cmd.GitGutterPreviewHunk,	'Preview Hunk'	},
			}
		}, { prefix = "<LEADER>" })
	end
}
