-- [[ Install ]]
return {
	'mbbill/undotree',
	-- [[ Configure ]]
	config = function()
		-- Keybindings
		vim.keymap.set('n', '<LEADER>u', vim.cmd.UndotreeToggle, { desc = 'UndoTree' })
	end
}
