-- [[ Install ]]
return {
	'nvim-lualine/lualine.nvim',
	dependencies = {
		'nvim-tree/nvim-web-devicons',
		'linrongbin16/lsp-progress.nvim'
	},
	-- [[ Configure ]]
	config = function()
		-- Settings
		require('lualine').setup {
			options = {
				icons_enabled = true,
				theme = 'auto',
				component_separators = { left = '│', right = '│' },
				-- component_separators = { left = '', right = '' },
				section_separators = { left = '', right = '' },
				-- section_separators = { left = '', right = '' },
				disabled_filetypes = { statusline = {}, winbar = {} },
				ignore_focus = {},
				always_divide_middle = true,
				globalstatus = true,
				refresh = {
					statusline	= 1000,
					tabline			= 1000,
					winbar			= 1000
				}
			},
			sections = {
				lualine_a = { 'mode' },
				lualine_b = { { 'branch', icon = '' }, 'diff' },
				lualine_c = { {
					'filename',
					file_status = true,
					path = 1,
					symbols = {
						modified	= '',
						readonly	= '',
						unnamed		= '',
						newfile		= '',
					}
				} },
				lualine_x = { { 'filetype', icon_only = false, }, "require('lsp-progress').progress()" },
				lualine_y = { {
					'diagnostics',
					symbols = {
						error	= ' ',
						warn	= ' ',
						info	= ' ',
						hint	= ' '
					}
				} },
				lualine_z = { 'location' }
			},
			inactive_sections = {
				lualine_a = {},
				lualine_b = {},
				lualine_c = { 'filename' },
				lualine_x = { 'location' },
				lualine_y = {},
				lualine_z = {}
			},
			-- tabline = {
			-- 	lualine_a = { {
			-- 		'buffers',
			-- 		use_mode_colors = true,
			-- 		symbols = {
			-- 			modified = ' ',
			-- 			alternate_file = '',
			-- 			directory = ''
			-- 		}
			-- 	} }
			-- },
			winbar = {},
			inactive_winbar = {},
			extensions = { 'quickfix' }
		}

		require('lsp-progress').setup({
			spinner = { '󰪞', '󰪟', '󰪠', '󰪡', '󰪢', '󰪣', '󰪤', '󰪥' }
		})
	end
}
