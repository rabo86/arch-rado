-- [[ Install ]]
return {
	'nvim-treesitter/nvim-treesitter',
	dependencies = {
		'nvim-treesitter/playground',
		'nvim-treesitter/nvim-treesitter-context',
		'HiPhish/rainbow-delimiters.nvim'
	},
	-- [[ Configure ]]
	config = function()
		-- Update TreeSitter
		pcall(require('nvim-treesitter.install').update { with_sync = true })

		-- Default Rainbow-Delimiters configuration
		---@type rainbow_delimiters.config
		vim.g.rainbow_delimiters = {
			strategy = {
				[''] = require('rainbow-delimiters').strategy['global'],
				vim = require('rainbow-delimiters').strategy['local'],
			},
			query = {
				[''] = 'rainbow-delimiters',
				lua = 'rainbow-blocks',
			},
			priority = {
				[''] = 110,
				lua = 210,
			},
			highlight = {
				'RainbowDelimiterRed',
				'RainbowDelimiterYellow',
				'RainbowDelimiterBlue',
				'RainbowDelimiterOrange',
				'RainbowDelimiterGreen',
				'RainbowDelimiterViolet',
				'RainbowDelimiterCyan',
			},
		}

		require('nvim-treesitter.configs').setup {
			-- Add languages to be installed here that you want installed for treesitter
			ensure_installed = {
				'bash',
				'c',
				'cmake',
				'cpp',
				'fortran',
				'lua',
				'make',
				'markdown',
				'python',
				'query',
				'vim',
				'vimdoc' },
			-- Install parsers synchronously (only applied to `ensure_installed`)
			sync_install = false,
			-- Automatically install missing parsers when entering buffer
			-- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
			auto_install = false,
			-- Highlight settings
			highlight	= {
				enable = true,
				-- Setting this to true will run `:h syntax` and tree-sitter at the same time.
				-- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
				-- Using this option may slow down your editor, and you may see some duplicate highlights.
				-- Instead of true it can also be a list of languages
				additional_vim_regex_highlighting	= false
			},
		}
	end
}
