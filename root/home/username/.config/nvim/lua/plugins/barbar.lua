return {
	'romgrk/barbar.nvim',
	event = 'BufAdd',
	dependencies = {
		-- 'lewis6991/gitsigns.nvim', -- OPTIONAL: for git status
		'nvim-tree/nvim-web-devicons', -- OPTIONAL: for file icons
		'folke/which-key.nvim'
	},
	init = function() vim.g.barbar_auto_setup = false end,
	opts = {
		auto_hide = true,
		icons = {
			button		= '',
			modified	= { button = '' },
			pinned		= { button = '', filename = true },
		},
		-- Keybindings
		vim.keymap.set('n', '<Tab>',			'<CMD>BufferNext<CR>',								{ desc = 'Switch to next buffer'													}),
		vim.keymap.set('n', '<S-Tab>',		'<CMD>BufferPrevious<CR>',						{ desc = 'Switch to previous buffer'											}),
		vim.keymap.set('n', '<A-Right>',	'<CMD>BufferMoveNext<CR>',						{ desc = 'Move current buffer for one spot to the right'	}),
		vim.keymap.set('n', '<A-Left>',		'<CMD>BufferMovePrevious<CR>',				{ desc = 'Move current buffer for one spot to the left'		}),
		vim.keymap.set('n', '<leader>x',	'<CMD>BufferClose<CR>',								{ desc = 'Close current buffer'														}),
		vim.keymap.set('n', '<leader>X',	'<CMD>BufferCloseAllButCurrent<CR>',	{ desc = 'Close all but current buffer'										})
	}
}
