return {
	'mfussenegger/nvim-dap',
	dependencies = {
		-- Creates a beautiful debugger UI
		'rcarriga/nvim-dap-ui',
		'nvim-telescope/telescope-dap.nvim',
		 -- Installs the debug adapters for you
    'williamboman/mason.nvim',
    'jay-babu/mason-nvim-dap.nvim',
		-- Add your own debuggers here
		'mfussenegger/nvim-dap-python',
	},
	keys = {
		{ '<LEADER>ab', function() require('dap').toggle_breakpoint() end,																		desc = 'Breakpoint'	},
		{ '<LEADER>ac', function() require('dap').set_breakpoint(vim.fn.input 'Breakpoint condition: ') end,	desc = 'Conditional breakpoint' }
	},
	config = function()
		-- Add red color to paint Dap signs
		vim.cmd('highlight DapRed guifg=#FF0000')
		vim.cmd('highlight DapYel guifg=#FFFF00')
		vim.cmd('highlight DapBlu guifg=#0000FF')
		vim.cmd('highlight DapGre guifg=#00FF00')
		-- vim.cmd('highlight DapSoftRed     guifg=#E45461')
		-- vim.cmd('highlight DapSoftYellow  guifg=#DBB46B')
		-- vim.cmd('highlight DapSoftBlue    guifg=#4AB0B9')
		-- vim.cmd('highlight DapSoftGreen   guifg=#8CBB6B')

		vim.fn.sign_define('DapBreakpoint',						{ text = '', texthl = 'DapRed' })
		-- vim.fn.sign_define('DapBreakpoint',						{ text = '', texthl = 'DapRed' })
		vim.fn.sign_define('DapBreakpointCondition',	{ text = 'ﳁ', texthl = 'DapRed' })
		-- vim.fn.sign_define('DapBreakpointCondition',	{ text = '', texthl = 'DapRed' })
		vim.fn.sign_define('DapStopped',							{ text = '', texthl = 'DapYel' })
		-- vim.fn.sign_define('DapStopped',							{ text = '', texthl = 'DapYel' })
		-- vim.fn.sign_define('DapStopped',							{ text = '', texthl = 'DapYel' })
		vim.fn.sign_define('DapBreakpointRejected',		{ text = '', texthl = 'DapRed' })
		vim.fn.sign_define('DapLogPoint',							{ text = '', texthl = 'DapBlu' })

		-- Basic debugging keymaps, feel free to change to your liking!
		vim.keymap.set('n', '<F1>', require('dap').step_into, { desc = 'Debug: Step Into' })
		vim.keymap.set('n', '<F2>', require('dap').step_over, { desc = 'Debug: Step Over' })
		vim.keymap.set('n', '<F3>', require('dap').step_out,	{ desc = 'Debug: Step Out'	})
		vim.keymap.set('n', '<F5>', require('dap').continue,	{ desc = 'Debug: Start/Continue' })
		vim.keymap.set('n', '<F7>', require('dapui').toggle,	{ desc = 'Debug: See the last Session Results' })

		-- Configure mason-nvim-dap
		require('mason-nvim-dap').setup {
      -- Makes a best effort to setup the various debuggers with
      -- reasonable debug configurations
      automatic_setup = true,

      -- You can provide additional configuration to the handlers,
      -- see mason-nvim-dap README for more information
      handlers = {},

      -- You'll need to check that you have the required things installed
      -- online, please don't ask me how to install them :)
      ensure_installed = {
        -- Update this to ensure that you have the debuggers for the langs you want
        'delve',
      },
    }

		-- Configure telescope-dap extension
		require('telescope').load_extension('dap')

		-- Dap UI setup
		-- For more information, see |:help nvim-dap-ui|
		require('dapui').setup {
			-- Set Dap UI visual appearance
			icons = {
				expanded			= '◢',
				collapsed			= '▶',
				current_frame	= '▶'
			},
			controls = {
				icons = {
					pause				= '  ',
					play				= '  ',
					step_into		= '  ',
					step_over		= '  ',
					step_out		= '  ',
					step_back		= '  ',
					run_last		= '  ',
					terminate		= '  ',
					disconnect	= '  '
				}
			},
			layouts = {
				{
					elements = {
						{ id = 'repl',        size = 0.01 },
						{ id = 'scopes',      size = 0.25 },
						{ id = 'breakpoints', size = 0.24 },
						{ id = 'stacks',      size = 0.24 },
						{ id = 'watches',     size = 0.25 }
					},
					position	= 'left',
					size			= 42
				},
				{
					elements	= { { id = 'console', size = 1.0 } },
					position	= 'bottom',
					size			= 10
				},
			}
		}

		require('dap').listeners.after.event_initialized['dapui_config'] = require('dapui').open
		require('dap').listeners.before.event_terminated['dapui_config'] = require('dapui').close
		require('dap').listeners.before.event_exited['dapui_config'] = require('dapui').close

		-- LANGUAGE CONFIGURATIONS
		-- Python
		require('dap-python').setup() -- This extension or all the commented lines below
		-- require('dap').adapters.python = function(cb, config)
		-- 	if config.request == 'attach' then
		-- 		---@diagnostic disable-next-line: undefined-field
		-- 		local port = (config.connect or config).port
		-- 		---@diagnostic disable-next-line: undefined-field
		-- 		local host = (config.connect or config).host or '127.0.0.1'
		-- 		cb({
		-- 			type = 'server',
		-- 			port = assert(port, '`connect.port` is required for a python `attach` configuration'),
		-- 			host = host,
		-- 			options = {
		-- 				source_filetype = 'python',
		-- 			},
		-- 		})
		-- 	else
		-- 		cb({
		-- 			type = 'executable',
		-- 			command = 'path/to/virtualenvs/debugpy/bin/python',
		-- 			args = { '-m', 'debugpy.adapter' },
		-- 			options = {
		-- 				source_filetype = 'python',
		-- 			},
		-- 		})
		-- 	end
		-- end
		-- require('dap').configurations.python = {
		-- 	{
		-- 		-- The first three options are required by nvim-dap
		-- 		type = 'python', -- the type here established the link to the adapter definition: `dap.adapters.python`
		-- 		request = 'launch',
		-- 		name = 'Launch file',
		--
		-- 		-- Options below are for debugpy, see https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for supported options
		--
		-- 		program = '${file}', -- This configuration will launch the current file if used.
		-- 		pythonPath = function()
		-- 			-- debugpy supports launching an application with a different interpreter then the one used to launch debugpy itself.
		-- 			-- The code below looks for a `venv` or `.venv` folder in the current directly and uses the python within.
		-- 			-- You could adapt this - to for example use the `VIRTUAL_ENV` environment variable.
		-- 			local cwd = vim.fn.getcwd()
		-- 			if vim.fn.executable(cwd .. '/venv/bin/python') == 1 then
		-- 				return cwd .. '/venv/bin/python'
		-- 			elseif vim.fn.executable(cwd .. '/.venv/bin/python') == 1 then
		-- 				return cwd .. '/.venv/bin/python'
		-- 			else
		-- 				return '/usr/bin/python'
		-- 			end
		-- 		end,
		-- 	},
		-- }
	end
}
