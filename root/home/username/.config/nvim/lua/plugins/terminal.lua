-- [[ Install ]]
return {
	'akinsho/toggleterm.nvim',
	version = '*',
	-- cmd = 'Terminal',
	keys = {
		{ '<LEADER><CR>', vim.cmd.ToggleTerm, desc = 'Open Terminal' }
	},
	-- [[ Configure ]]
	opts = {
		direction = 'float',
		autochdir = false,
		float_opts = {
			border = 'double',
			-- width = 80,
			-- height = 55,
		},
	}
}
