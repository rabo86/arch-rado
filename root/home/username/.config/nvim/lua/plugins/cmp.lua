return {
	-- Install Autocompletion & Snippets
	'hrsh7th/nvim-cmp', -- Required
	event = 'InsertEnter',
	dependencies = {
		-- Autocompletion
		'hrsh7th/cmp-nvim-lsp',      -- Required
		'hrsh7th/cmp-buffer',        -- Optional
		'hrsh7th/cmp-path',          -- Optional
		'hrsh7th/cmp-nvim-lua',      -- Optional
		'hrsh7th/cmp-cmdline',       -- Optional
		'saadparwaiz1/cmp_luasnip',  -- Optional
		-- Snippets
		'L3MON4D3/LuaSnip',          -- Required
		'rafamadriz/friendly-snippets' -- Optional
	},
	-- [[ Configure Autocompletion & Snippets ]]
	config = function()
		-- Lazy load snippets from VS Code
		-- Check if we need this
		require('luasnip/loaders/from_vscode').lazy_load()

		-- Improve SuperTab
		local check_backspace = function()
			local col = vim.fn.col '.' - 1
			return col == 0 or vim.fn.getline('.'):sub(col, col):match '%s'
		end

		-- Adding icons to the completion menu
		local kind_icons = {
			Array					= '',
			Boolean				= '',
			Class					= '',
			Color					= '',
			Constant			= '',
			Constructor		= '',
			Copilot				= '',
			Enum					= '',
			EnumMember		= '',
			Event					= '',
			Field					= '',
			File					= '',
			Folder				= '',
			Function			= '',
			Interface			= '',
			Keyword				= '',
			Method				= '󰦆',
			Module				= '',
			Namespace			= '',
			Null					= '󰟢',
			Number				= '',
			Object				= '',
			Operator			= '',
			Package				= '',
			Property			= '',
			Reference			= '',
			Snippet				= '',
			String				= '',
			Struct				= '',
			Text					= '',
			TypeParameter	= '',
			Unit					= '',
			Value					= '',
			Variable			= ''
		}
		-- NVIM-CMP
		require('luasnip').config.setup {}

		require('cmp').setup {
			snippet = {
				expand = function(args)
					require('luasnip').lsp_expand(args.body)
				end
			},
			mapping = require('cmp').mapping.preset.insert {
				['<C-d>'] = require('cmp').mapping.scroll_docs(-4),
				['<C-f>'] = require('cmp').mapping.scroll_docs(4),
				['<C-Space>'] = require('cmp').mapping.complete {},
				['<CR>'] = require('cmp').mapping.confirm {
					behavior = require('cmp').ConfirmBehavior.Replace,
					select = true,
				},
				['<Esc>'] = require('cmp').mapping.abort(),
				['<Tab>'] = require('cmp').mapping(function(fallback)
					if require('cmp').visible() then
						require('cmp').select_next_item()
					elseif require('luasnip').expandable() then
						require('luasnip').expand()
					elseif require('luasnip').expand_or_jumpable() then
						require('luasnip').expand_or_jump()
					elseif check_backspace() then
						fallback()
					else
						fallback()
					end
				end, { 'i', 's' }),
				['<S-Tab>'] = require('cmp').mapping(function(fallback)
					if require('cmp').visible() then
						require('cmp').select_prev_item()
					elseif require('luasnip').jumpable(-1) then
						require('luasnip').jump(-1)
					else
						fallback()
					end
				end, { 'i', 's' })
			},
			formatting = {
				fields = { 'kind', 'abbr', 'menu' },
				format = function(entry, vim_item)
					-- Kind icons
					vim_item.kind = string.format('%s', kind_icons[vim_item.kind])
					-- Kind icons & name
					-- vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind)
					vim_item.menu = ({
						buffer		= '﬘',
						luasnip		= '',
						nvim_lsp	= '',
						nvim_lua	= '',
						path			= ''
					})[entry.source.name]
					return vim_item
				end
			},
			sources = {
				{ name = 'nvim_lua' },
				{ name = 'nvim_lsp' },
				{ name = 'buffer' },
				{ name = 'luasnip' },
				{ name = 'path' }
			},
			window = {
				completion = {
					-- border = { '╓', '─', '╖', '║', '╜', '─', '╙', '║' }
					border = { '╒', '═', '╕', '│', '╛', '═', '╘', '│' }
					-- border = { '┏', '━', '┓', '┃', '┛', '━', '┗', '┃' }
				},
				documentation = {
					border = { '╓', '─', '╖', '║', '╜', '─', '╙', '║' }
					-- border = { '╒', '═', '╕', '│', '╛', '═', '╘', '│' }
					-- border = { '╔', '═', '╗', '║', '╝', '═', '╚', '║' }
					-- border = { '┏', '━', '┓', '┃', '┛', '━', '┗', '┃' }
				}
			},
			duplicates = {
				buffer		= 1,
				path			= 1,
				nvim_lsp	= 0,
				luasnip		= 1
			},
			duplicates_default = 0,
			experimental = {
				ghost_text	= true,
				native_menu = false
			}
		}

		-- CMP-CMDLINE
		-- CmdLine autocompletion on search (/)
		require('cmp').setup.cmdline('/', {
			mapping = require('cmp').mapping.preset.cmdline(),
			sources = { { name = 'buffer' } }
		})
		-- CmdLine autocompletion on command (:)
		require('cmp').setup.cmdline(':', {
			mapping = require('cmp').mapping.preset.cmdline(),
			sources = require('cmp').config.sources({
				{ name = 'path' }
			}, {
				{
					name = 'cmdline',
					option = {
						ignore_cmds = { 'Man', '!' }
					}
				}
			})
		})
	end
}
