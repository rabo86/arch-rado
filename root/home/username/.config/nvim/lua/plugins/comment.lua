-- [[ Install ]]
return {
	'numToStr/Comment.nvim',
	cmd = 'Comment',
	keys = {
		{ 'gc', mode = { 'n', 'x', 'o' }, desc = 'Comment linewise'	 },
		{ 'gb', mode = { 'n', 'x', 'o' }, desc = 'Comment blockwise' },
	},
	-- [[ Configure ]]
	config = function()
		require('Comment').setup()
	end
}
