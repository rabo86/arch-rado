return {
	-- [[ Install ]]
	'neovim/nvim-lspconfig',
	dependencies = {
		'folke/neodev.nvim',
		'folke/which-key.nvim',
		'nvim-telescope/telescope.nvim'
	},
	-- [[ Configure ]]
	config = function()
		-- Load LSP icons
		local signs = {
			Error	= '',
			Warn	= '',
			-- Warn	= '',
			Hint	= '',
			-- Hint	= '',
			Info	= ''
		}
		for type, icon in pairs(signs) do
			local hl = 'DiagnosticSign' .. type
			vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
		end

		-- Keybindings for every single buffer that has LSP associated with it
		local on_attach = function(_, bufnr)
			require("which-key").register({
				-- Buffer
				b = { name = 'Buffer',
					a = { vim.lsp.buf.code_action,	'Select a Code Action'	},
					f = { vim.lsp.buf.format,				'Format'								}
				},
				-- Symbols/Variables
				s = { name = 'Symbols & Refs',
					b = { require('telescope.builtin').lsp_document_symbols,					'List Symbols in Buffer'				},
					c = { vim.lsp.buf.rename,																					'Change/Rename'									},
					d = { require('telescope.builtin').lsp_definitions,								'Jump to Definition'						},
					h = { vim.lsp.buf.hover,																					'Hover Information'							},
					i = { require('telescope.builtin').lsp_implementations,						'List Implementations'					},
					r = { require('telescope.builtin').lsp_references,								'List References in Workspace'	},
					w = { require('telescope.builtin').lsp_dynamic_workspace_symbols,	'List Symbols in Workspace'			}
				}
			}, { prefix = "<LEADER>", buffer = bufnr })
		end

		-- Configure Neovim LUA setup, help, docs & completion
		require('neodev').setup()

		-- Set handlers for installed LSPs
		local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities()) -- Broadcast additional nvim-cmp completion capabilities to the LSPs
		require('lspconfig').bashls.setup {
			capabilities = capabilities,
			on_attach = on_attach,
		}
		require('lspconfig').clangd.setup {
			capabilities = capabilities,
			on_attach = on_attach,
		}
		require('lspconfig').cmake.setup {
			capabilities = capabilities,
			on_attach = on_attach,
		}
		require('lspconfig').fortls.setup {
			capabilities = capabilities,
			on_attach = on_attach,
		}
		require('lspconfig').lua_ls.setup {
			capabilities = capabilities,
			on_attach = on_attach,
			settings = {
				Lua = {
					diagnostics = {
						globals = { 'vim' },
						disable = { "missing-parameters" , "missing-fields" }
					},
					workspace = { checkThirdParty = false },
					telemetry = { enable = false }
				}
			}
		}
		require('lspconfig').pyright.setup {
			capabilities = capabilities,
			on_attach = on_attach,
		}
		require('lspconfig').vimls.setup {
			capabilities = capabilities,
			on_attach = on_attach,
		}
	end
}
