-- [[ Install ]]
return {
	'tpope/vim-fugitive',
	dependencies = {
		'folke/which-key.nvim'
	},
	-- cmd = 'GitFugitive',
	ft = { 'gitcommit', 'diff' }, -- git filetypes
	init = function()
		-- Load GitFugitive only when a git file is opened
		vim.api.nvim_create_autocmd({ 'BufRead' }, {
			group = vim.api.nvim_create_augroup('GitFugitiveLazyLoad', { clear = true }),
			callback = function()
				vim.fn.system('git -C ' .. '"' .. vim.fn.expand '%:p:h' .. '"' .. ' rev-parse')
				if vim.v.shell_error == 0 then
					vim.api.nvim_del_augroup_by_name 'GitFugitiveLazyLoad'
					vim.schedule(function()
						require('lazy').load { plugins = { 'vim-fugitive' } }
					end)
				end
			end,
		})
	end,
	-- [[ Configure ]]
	config = function()
		-- Keybindings
		require("which-key").register({
			-- Buffer
			g = { name = 'Git',
				g = { vim.cmd.Git, 'Fugitive' },
			}
		}, { prefix = "<LEADER>" })
	end
}
