-- [[ Install ]]
return {
	'norcalli/nvim-colorizer.lua',
	-- [[ Configure ]]
	config = function()
		require('colorizer').setup()
	end
}
