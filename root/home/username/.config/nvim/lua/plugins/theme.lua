-- [[ Install ]]
return {
	'navarasu/onedark.nvim',
	priority = 1000,
	-- [[ Configure ]]
	config = function()
		--vim.cmd.colorscheme 'onedark'
		require('onedark').setup {
			style = 'darker'
		}
		require('onedark').load()
	end
}
