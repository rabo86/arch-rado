-- [[ Install ]]
return {
	'folke/trouble.nvim',
	-- cmd = 'Diagnostic',
	keys = {
		{ '<LEADER>dd', '<CMD>TroubleToggle document_diagnostics<CR>',	desc = 'Document'				},
		{ '<LEADER>dl', '<CMD>TroubleToggle loclist<CR>',								desc = 'Location list'	},
		{ '<LEADER>dq', '<CMD>TroubleToggle quickfix<CR>',							desc = 'QuickFix'				},
		{ '<LEADER>dr', '<CMD>TroubleToggle lsp_references<CR>',				desc = 'LSP References'	},
		{ '<LEADER>dt', vim.cmd.TroubleToggle,													desc = 'Toggle'					},
		{ '<LEADER>dw', '<CMD>TroubleToggle workspace_diagnostics<CR>',	desc = 'Workspace'			},
		{ '<LEADER>dx', vim.cmd.TroubleClose,														desc = 'Close'					}
	},
	dependencies = { 'nvim-tree/nvim-web-devicons' },
	-- [[ Configure ]]
	config = function()
		-- Change the icons used for diagnostic
		require('trouble').setup {
			signs = {
				-- icons / text used for a diagnostic
				error				= "",
				warning			= "",
				hint				= "",
				information	= "",
				other				= ""
			}
		}

		-- Additional keybindings
		-- jump to the next item, skipping the groups
		require('trouble').next({ skip_groups = true, jump = true });
		-- jump to the previous item, skipping the groups
		require('trouble').previous({ skip_groups = true, jump = true });
		-- jump to the first item, skipping the groups
		require('trouble').first({ skip_groups = true, jump = true });
		-- jump to the last item, skipping the groups
		require('trouble').last({ skip_groups = true, jump = true });

		require('telescope').setup {
			defaults = {
				mappings = {
					i = { ['<c-t>'] = require('trouble.providers.telescope').open_with_trouble },
					n = { ['<c-t>'] = require('trouble.providers.telescope').open_with_trouble }
				}
			}
		}
	end
}
