-- [[ Install ]]
return {
	'folke/which-key.nvim',
	-- [[ Configure ]]
	config = function()
		-- Settings
		vim.o.timeout = true
		vim.o.timeoutlen = 0
		require('which-key').setup {
			-- your configuration comes here
			-- or leave it empty to use the default settings
			-- refer to the configuration section below
			key_labels = {
				-- override the label used to display some keys. It doesn't effect WK in any other way.
				-- For example:
				["<bs>"]			= "BACKSPACE",
				["<CR>"]			= "RETURN",
				["<esc>"]			= "ESCAPE",
				["<leader>"]	= "SPACE", --change this line if the LEADER key changes from SPACE
				["<space>"]		= "SPACE",
				["<Tab>"]			= "TAB",
			},
		}
		require("which-key").register({
			a = { name = 'Add' },
			d = { name = 'Diagnostic' },
			i = { name = 'Indent Blankline' },
			t = { name = 'Tree' },
		}, { prefix = "<LEADER>" })
	end
}
