return {
	-- [[ Install ]]
	'williamboman/mason.nvim',
	-- [[ Configure ]]
	config = function()
		-- Setup MASON, so it can manage external tooling
		require('mason').setup({
			ui = {
				icons = {
					package_installed		= '',
					package_pending			= '',
					package_uninstalled	= ''
				}
			}
		})

		-- List of all language tools to be installed if missing
		local ensure_installed = {
			-- LSPs
			'bash-language-server',
			'clangd',
			'cmake-language-server',
			'fortls',
			'lua-language-server',
			'pyright',
			'vim-language-server',
			-- DAPs
			'bash-debug-adapter',
			'cpptools',
			'debugpy',
			-- Linters	
			'shellcheck',
		}

		-- Search for the non-installed LSPs from the list
		local not_installed = {}
		for _, v in pairs(ensure_installed) do
			if not (require('mason-registry').is_installed(v)) then
				table.insert(not_installed, v)
			end
		end
		-- Install non-installed LSPs if any
		if (#not_installed > 0) then
			vim.cmd('MasonInstall ' .. table.concat(not_installed, ' '))
		end

		-- Keybindings
		vim.keymap.set('n', '<leader>m', vim.cmd.Mason, { desc = 'Mason' })
	end
}
