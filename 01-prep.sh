#!/bin/bash
# @file			01-prep.sh
# @brief		This script will partition, format and mount all partitions selected in the install.sh
#						script. In case, two separate disks/drives of different type were selected for ROOT and
#						HOME partitions	and the filesystem type is BTRFS, we will have to create just ROOT
#						partition	and mount HOME as part of ROOT partition.
#	@args			1: ROOT disk/drive
#						2: HOME disk/drive (if selected, otherwise ROOT disk/drive)
#						3: FileSystem	(EXT4 or BTRFS)
#						4: LUKS Password (if entered, otherwise the fourth argument does not exist)
#▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬
#																							PARTITION
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
# Rename the partitions depending of the type of the ROOT disk (SSD, flash memory, etc.)
if [[ "$1" =~ "nvme" ]]; then
    DISK1=${1}p1
    DISK2=${1}p2
    DISK3=${1}p3
		if [ "$3" == 'btrfs' ]; then
			MOUNTOPTS3="noatime,compress=zstd,ssd,commit=120"
		fi
else
    DISK1=${1}1
    DISK2=${1}2
    DISK3=${1}3
		if [ "$3" == 'btrfs' ]; then
			MOUNTOPTS3="noatime,compress=zstd,commit=120"
		fi
fi

# Make sure to unmount everything before start
umount -A --recursive /mnt

# Delete partitions on root disk and set new GPT disk 2048 alignment
sgdisk -Z "$1"
sgdisk -a 2048 -o "$1"

# Create partitions: BOOT (512MiB), SWAP (8GiB) & ROOT (the rest)
sgdisk -n 1::+512M	--typecode=1:ef00 --change-name=1:'boot' "$1"
sgdisk -n 2::+8G		--typecode=2:8200 --change-name=2:'swap' "$1"
sgdisk -n 3::0			--typecode=3:8300 --change-name=3:'root' "$1"

# Reload partition table on ROOT disk
partprobe "$1"

# Separate home partitions
if [ ! "$1" == "$2" ]; then
	# Rename the partitions depending of the type of the HOME disk (SSD, flash memory, etc.)
	if [[ "$2" =~ 'nvme' ]]; then
		DISK4=${2}p1
		# MOUNTOPTS4="noatime,compress=zstd,ssd,commit=120"
	else
		DISK4=${2}1
		# MOUNTOPTS4="noatime,compress=zstd,commit=120"
	fi
	# Create HOME partition
	sgdisk -Z "$2"
	sgdisk -a 2048 -o "$2"
	sgdisk -n 1::0 --typecode=1:8302 --change-name=1:'home' "$2"
	# Reload partition table on HOME disk
	partprobe "$2"
fi

#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
#																			     FORMAT & MOUNT
#⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
# LUKS encryption if password exists
if [ -n "$4" ]; then
	# Enter LUKS password to cryptsetup and format ROOT partition
	echo -n "$4" | cryptsetup luksFormat "$DISK3" -
	# Open LUKS container and ROOT will be place holder
	echo -n "$4" | cryptsetup open "$DISK3" root -
	# Change the ROOT partition location to the encrypted space
	DISK3='/dev/mapper/root'
	if [ -n "$DISK4" ]; then
		# Enter LUKS password to cryptsetup and format HOME partition
		echo -n "$4" | cryptsetup luksFormat "$DISK4" -
		# Open LUKS container and HOME will be place holder
		echo -n "$4" | cryptsetup open "$DISK4" home -
		# Change the HOME partition location to the encrypted space
		DISK4='/dev/mapper/home'
	fi
fi

# Format and mount ROOT and HOME partition
if [ "$3" == 'ext4' ]; then
	# EXT4
	yes | mkfs -t ext4 -L ROOT "$DISK3"
	mount -t ext4 "$DISK3" /mnt
	if [ -n "$DISK4" ]; then
		yes | mkfs -t ext4 -L HOME "$DISK4"
		# mkdir -p /mnt/home
		mount -m -t ext4 "$DISK4" /mnt/home
	fi
elif [ "$3" == 'btrfs' ]; then
	# BTRFS
	yes | mkfs -t btrfs -L ROOT -f "$DISK3"
	mount -t btrfs "$DISK3" /mnt
	# Create the BTRFS subvolumes
  btrfs subvolume create /mnt/@
  btrfs subvolume create /mnt/@home
  btrfs subvolume create /mnt/@opt
  btrfs subvolume create /mnt/@tmp
  btrfs subvolume create /mnt/@var
  btrfs subvolume create /mnt/@snapshots
	# Unmount ROOT to remount with subvolume
	umount /mnt
	# Mount @ subvolume
	mount			-o "$MOUNTOPTS3",subvol=@						"$DISK3" /mnt
	# Mount all BTRFS subvolumes after ROOT has been mounted
	mount -m	-o "$MOUNTOPTS3",subvol=@home				"$DISK3" /mnt/home
	mount -m	-o "$MOUNTOPTS3",subvol=@opt				"$DISK3" /mnt/opt
	mount -m	-o "$MOUNTOPTS3",subvol=@tmp				"$DISK3" /mnt/tmp
	mount -m	-o "$MOUNTOPTS3",subvol=@var				"$DISK3" /mnt/var
	mount -m	-o "$MOUNTOPTS3",subvol=@snapshots	"$DISK3" /mnt/.snapshots
fi

# Format and mount BOOT & SWAP partition
mkfs.vfat -F32 -n BOOT "$DISK1"
# mkdir -p /mnt/boot
mount -m -t vfat -L BOOT /mnt/boot
mkswap -L SWAP "$DISK2"
swapon "$DISK2"
#▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬
