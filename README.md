# ABOUT THIS CONFIG
In this repository you will find packages-scripts for the base install of Arch Linux with Gnome, Openbox and Xmonad desktop environments. Remember that the first part of the Arch Linux install is manual, that is you will have to partition, format and mount the disk yourself. Install the base packages and make sure to inlcude: git to clone the repository in chroot, vim to edit files if necessary and grep to distinguish packages from comments in pkglist.txt files.

## Load your keymaps
```shell
loadkeys us #default
loadkeys dvorak #my keyboard layout
```

## Check internet connection
```shell
ip -c a #check possible ways to connect to internet: ethernet cable or wifi adapter
iwctl #we have to use IW utility to connect to a wifi network in case of a wifi connection
station <name_of_wifi_adapter_listed_in_the_first_command> <one of the outputs of "ip -c a", e.g. wlan0*> connect <name_of_the_network>
#enter the password for the chosen network
pacman -Sy #test/check by trying to synnc the repositories
```

## List all partitions
```shell
lsblk
```

## Partition the disk
```shell
cgdisk /dev/disk_name
```

## Format the partitions
```shell
mkfs.vfat /dev/efi_partition_name
mkfs.ext4 /dev/root_partition_name
```

## Create swap partition (optional)
```shell
mkswap /dev/swap_partition_name
swapon /dev/swap_partition_name
```

## Mount the partitions
```shell
mount /dev/root_partition_name /mnt
mkdir -p /mnt/boot
mount /dev/efi_partition_name /mnt/boot
```

## Format and mount the home directory at separate partition/disk (optional)
```shell
mkfs.ext4 /dev/separate_partition_name
mkdir -p /mnt/home
mount /dev/separate_partition_name /mnt/home
```

## Install the base packages into /mnt
Before installing the packages make sure the keyring in up to date.
```shell
pacman -Sy archlinux-keyring
pacstrap /mnt base linux linux-firmware git grep vim amd-ucode #or intel-ucode in case of intel CPU
```

## Generate the FSTAB file
```shell
genfstab -U /mnt >> /mnt/etc/fstab
```

## Chroot to /mnt
```shell
arch-chroot /mnt
```

## Clone git repository (from one of the three links below)
```shell
git clone https://gitlab.com/rabo86/arch-install.git
```

## In case of different username, content of some files has to be changed
   - base.sh (**radovan**--><_username_> and **beast**--><_comupter_name_>)
   - xmonad/.config/gtk-3.0/bookmarks (**radovan**--><_username_>)
   - xmonad/.config/qBittorrent/qBittorrent.conf (**radovan**--><_username_>)

## Run shell scripts
```shell
sh /arch-install/base.sh
nmtui #in case of wireless connection run this command to connect to the network
sh /arch-install/wm/wmanager.sh openbox   #openbox installation
sh /arch-install/wm/wmanager.sh xmonad    #xmonad installation
```
If you want to install 32-bit software and libraries add 32 as second argument:
```shell
sh /arch-install/wm/wmanager.sh openbox 32
sh /arch-install/wm/wmanager.sh xmonad 32
```
If any other string as second argument has been provided, the installation will continue without 32-bit software/packages.

## Run to update powerlevel10k theme
```shell
git -C ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k pull
```

## Install Intel MKL
Before running the set of commands below, check if there is maybe a newer version by opening this link: https://www.intel.com/content/www/us/en/developer/tools/oneapi/onemkl-download.html?operatingsystem=linux&distributions=online
```shell
wget https://registrationcenter-download.intel.com/akdlm/irc_nas/19138/l_onemkl_p_2023.0.0.25398.sh
sudo sh ./l_onemkl_p_2023.0.0.25398.sh
```

## Procedure to reset the keys in case of signature issues
```shell
sudo rm -r /etc/pacman.d/gnupg
sudo pacman-key --init
sudo pacman-key --populate archlinux
sudo pacman -Sy gnupg archlinux-keyring
sudo pacman-key --refresh-keys
```

## Encrypt a file using GPG
```shell
gpg -c <_file_name_>
gpg -d <_file_name_>
```

## Clean Arch Linux cache
```shell
(sudo) ls /var/cache/pacman/pkg/ | less #list of the packages
(sudo) ls /var/cache/pacman/pkg/ | wc -l #count the packages
du -sh /var/cache/pacman/pkg/ #size of the packages
sudo pacman -Sc #remove all uninstalled packages from cache
sudo pacman -Scc #remove all packages (installed + uninstalled) from cache
```

## Remove unused/orphan packages
```shell
sudo pacman -Qtdq #list orphans
sudo pacman -Rns $(pacman -Qtdq) #remove orphans with the dependances
cleanup #alias in .zshrc to clean the cache and remove orphans with the dependencies
```
